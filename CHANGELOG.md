## 0.20.0 (2024-11-10)

- Python3.9のサポートをドロップした
- 推奨バージョンはPython3.11以上

### Fix

- **poetry.lock**: lockファイルを更新した
- **pyproject.toml**: pythonのバージョンを更新した: >=3.10

## 0.19.3 (2024-11-02)

サポートツール周りを改善した

### Fix

- **.pre-commit-config.yaml**: hookの順番を変更した
- **.pre-commit-config.yaml**: hookを更新した: commitizen: 2.24.0 -> 3.29.0
- **.pre-commit-config.yaml**: hookを追加した: check-added-large-files
- **.pre-commit-config.yaml**: hookを追加した: check-case-conflict
- **.pre-commit-config.yaml**: hookを追加した: check-merge-conflict
- **.pre-commit-config.yaml**: hookを追加した: check-toml
- **.pre-commit-config.yaml**: hookを追加した: check-yaml
- **.pre-commit-config.yaml**: hookを追加した: detect-private-key
- **.pre-commit-config.yaml**: hookを追加した: end-of-file-fixer
- **.pre-commit-config.yaml**: hookを追加した: name-tests-test
- **.pre-commit-config.yaml**: hookを追加した: ruff-format
- **.pre-commit-config.yaml**: hookを追加した: trailing-whitespace
- **mystmd/logbook/run50/20240830_run50_adc.ipynb**: ADC分布を確認した
- **mystmd/logbook/run50/20240830_run50_adc.ipynb**: clear output
- **mystmd/logbook/run50/20240830_run50_adc.ipynb**: fixed
- **mystmd/logbook/run50/20240830_run50_adc.ipynb**: 長期測定したときのADC分布を確認した

### Refactor

- **.gitlab-ci.yml**: add orders
- **.gitlab-ci.yml**: cleaned CI
- **.gitlab-ci.yml**: fixed before_script

### Dependencies

- **.pre-commit-config.yaml**: hooksを更新した
- **pyproject.toml**: commitizenを更新した: 3.12.0 -> 3.30.0
- **pyproject.toml**: pytestを更新した: 8.2.1 -> 8.3.3
- **pyproject.toml**: ruffを更新した: 0.6.9 -> 0.7.2
- **poetry.lock**: パッケージを更新した


## 0.19.2 (2024-08-30)

### Fix

- **haniwers/cli.py:version**: poetryを使うアドバイスを削除した
- **haniwers/cli.py:version**: 確認する情報を追加した

## 0.19.1 (2024-08-24)

### Fix

- **haniwers/cli.py:_setup_logger**: プライベート関数（sunder）に変更した

## 0.19.0 (2024-08-24)

### Feat

- **haniwers/cli.py**: 新しいコマンド：docsコマンドを追加した
- **haniwers/cli.py**: 既存コマンド：docsオプションを追加した

### Fix

- **haniwers/cli.py**: webbrowserを追加した
- **haniwers/cli.py**: 既存コマンドのdocsオプションをtyper推奨のアノテーションにした
- **haniwers/cli.py**: 既存コマンドの引数にアノテーションを追加した

### Notebooks

- **mystmd/logbook/run85/run85_zenith.ipynb**: RunManagerを使った方法に修正した
- **mystmd/logbook/run95/run95_zenith.ipynb**: 修正した
- **mystmd/_toc.yml**: 削除した

### Test

- **haniwers/config.py:RunManager**: メンバー変数にis_test=Falseを追加した

### Refactor

- **.gitattributes**: ipynbをGit LFSに追加した
- **haniwers/cli.py**: コマンドの順番を整理した
- **mystmd/logbook/**: Git LFSオブジェクトに変換した
- **notebooks/**: Git LFSオブジェクトに変換した

### Dependency

- **pyproject.toml**: mystmdを更新した: 1.3.4 -> 1.3.5

## 0.18.0 (2024-08-23)

### Feat

- **sandbox/runs.csv**: ラン情報のCSVファイルを追加した
- **haniwers/cli.py**: ConfigモジュールをRunManagerモジュールで置き換えた

### Fix

- **data/parsed/20240817**: さじアストロパークのデータを追加した
- **haniwers/cli.py:run2csv**: driveオプションを追加した
- **haniwers/config.py**: Configクラスをdeprecatedにしたい
- **haniwers/config.py**: RunManagerを修正した
- **haniwers/config.py**: データのパスを変更できるようにした
- **haniwers/config.py**: レコード取得のメソッドを整理した
- **haniwers/dataset.py:_get_path**: 共通部分を _get_path に整理した
- **tests/runs.csv**: シンボリックリンクから実態に変更した
- **tests/runs.csv**: パスを修正した

### Refactor

- **haniwers/config.py**: 削除予定のメッセージを追加した

### Notebooks

- **mystmd/logbook/run95/run95_zenith.ipynb**: さじアストロパークでのデータを解析した
- **notebooks/50_config_runmanager.ipynb**: RunManagerクラスのプロトタイプを作成した
- **notebooks/50_config_runmanager.ipynb**: スプレッドシートの形式を修正した
- **notebooks/57_preprocess_run2csv.ipynb**: これまでのConfigとRunManagerをどう繋げるかを考え中

## 0.17.2 (2024-08-19)

### Dependency

- **poetry.lock**: パッケージを更新した
- **pyproject.toml**: ipykernelとruffを更新した
- **pyproject.toml**: polarsを更新した（0.20.31 -> 1.5.0）

### Style

- **mystmd/logbook/**: logbookをruff formatした
- **mystmd/tutorials/**: tutorialsをruff formatした
- **notebooks/**: notebooksをruff formatした

## 0.17.1 (2024-07-30)

### Fix

- **poetry.lock**: パッケージを更新した
- **pyproject.toml**: pyarrowを更新した: 15.0.2 -> 16.1.0

## 0.17.0 (2024-06-12)

### Feat

- **haniwers/daq.py:run_daq**: ポート設定とDAQ設定を分離した

### Fix

- **haniwers/cli.py:mock_daq**: DAQのモックを作成中
- **haniwers/daq.py:run_daq2**: テスト用のDAQ関数を追加した
- **haniwers/dataset.py:ondotori_data**: おんどとりのデータを読み込む部分を修正した

### Notebooks

- **mystmd/logbook/run85_zenith.ipynb**: 天頂角分布測定の解析ノートを追加した
- **mystmd/logbook/run91.ipynb**: Run91の解析ノートを追加した

### Dataset

- **data/parsed/20240531_run79.csv**: Run79を追加した
- **data/parsed/20240601_run80.csv**: Run80を追加した
- **data/parsed/20240603_run85.csv**: Run85を追加した
- **data/parsed/20240603_run86.csv**: Run86を追加した
- **data/parsed/20240605_run87.csv**: Run87を追加した
- **data/parsed/20240606_run88.csv**: Run88を追加した
- **data/parsed/20240607_run89.csv**: Run89を追加した
- **data/parsed/20240608_run90.csv**: Run90を追加した
- **data/parsed/20240609_run91.csv**: Run91を追加した
- **data/parsed/20240610_run92.csv**: Run92を追加した
- **data/parsed/20240611_run93.csv**: Run93を追加した

## 0.16.0 (2024-06-01)

### Feat

- **haniwers/daq.py:run_daq**: _read_eventを使うフローに切り替えた
- **haniwers/mimic.py:FakeEvent**: to_mock_stringを追加した
- **haniwers/daq.py:_loop_and_save_events**: イベント保存ループを追加した
- **haniwers/daq.py:_loop_events_for_rows**: 回数指定のループを追加した
- **haniwers/daq.py:_loop_events_for_duration**: 時間指定のループを追加した
- **haniwers/daq.py:scan_daq**: スレッショルド測定用のDAQを追加した

### Fix

- **haniwers/cli.py:setup_logger**: デバッグ時はlong formatでstderrに表示することにした Changelog: changed
- **haniwers/cli.py:version**: ログファイル名の表示を追加した
- **haniwers/daq.py:_loop_and_save_events**: ファイルを開くモードを x に変更した
- **haniwers/daq.py:_loop_and_save_events**: ファイル名を引数にした Changelog: changed
- **haniwers/daq.py:_loop_events_for_duration**: デバッグ表示を追加した
- **haniwers/daq.py:_loop_events**: 返り値の型ヒントを追加した
- **haniwers/daq.py:_read_event**: 関数名を変更した Changelog: changed
- **haniwers/daq.py:loop_and_save_events**: 削除予定
- **haniwers/daq.py:loop_and_save**: ジェネレーターを引数に使い汎用的にした Changelog: added
- **haniwers/daq.py:loop_files**: 指定したファイル数のループを追加した
- **haniwers/preprocess.py:read_data_with_polars**: スレッショルド測定のデータを読み込めるようにした Changelog: changed
- **haniwers/threshold.py:scan_threshold**: スレッショルド測定のデータをデータフレームに変換した

### Removed

- **haniwers/daq.py:_loop_events**: 削除した Changelog: removed
- **haniwers/daq.py:loop_and_save**: 引数からsuffixを削除した


### Refactor

- **haniwers/daq.py**: 削除予定の関数を整理した

### Dataset

- **data/parsed/20240528_run75.csv**: Run75を追加した
- **data/parsed/20240529_run76.csv**: Run76を追加した
- **data/parsed/20240529_run77.csv**: Run77を追加した
- **data/parsed/20240530_run78.csv**: Run78を追加した

### Notebooks

- **mystmd/logbook/run76/20240529_run76.ipynb**: Run76を追加した
- **mystmd/tutorials/03-pyserial.ipynb**: PySerialとMagicMockの使い方を追加した
- **mystmd/tutorials/03-pyserial.ipynb**: pyserialのチュートリアルを追加した
- **notebooks/10_quick_threshold_fit.ipynb**: Run76のスレッショルド測定を追加した
- **notebooks/61_mock_daq.ipynb**: シリアル通信のmockを作成した

## 0.15.2 (2024-05-29)

`platformdirs`を依存パッケージに追加しました

### Dataset

- **data/parsed/20240527_run74.csv**: Run74を追加した

### Notebooks

- **mystmd/logbook/run50/20240507_run50.ipynb**: 気温による補正を二次関数にしてみた

### Dependency

- **pyproject.toml**: platformdirsを追加した

## 0.15.1 (2024-05-28)

ログファイルを保存するパスを ``platformdirs`` の定番ディレクトリに変更しました。
また、CLIののサブコマンドに``--log-level``オプションを追加し、``--append``オプションを削除しました。

### Fix

- **haniwers/cli.py:setup_logger**: ログファイルの保存先を platformdirs の定番パスに変更した
- **haniwers/cli.py**: appendオプションや設定を削除した
- **haniwers/cli.py**: log_levelオプションを追加した
- **haniwers/daq.py:init_saved**: mkdir_savedに置き換えた
- **haniwers/daq.py:init_saved**: ディレクトリがすでに存在していてもmkdirできるようにした

## 0.15.0 (2024-05-27)

DAQの内部処理を変更しました。
RealEventクラスで受け取ってからファイルに保存することで、測定した値のvalidationができるようにしました。
ファイルの出力内容は同じなので、これまでの解析フローに影響はありません。

### Feat

- **haniwers/daq.py:_read_serial_data_as_event**: RealEventクラスでデータを受け取ることにした
- **haniwers/daq.py:loop_and_save_events**: 測定データを保存する方法を追加した
- **haniwers/daq.py:loop_events**: イベント取得のループを追加した

### Fix

- **data/parsed/20240524_run68.csv**: Run68を追加した
- **data/parsed/20240525_run70.csv**: Run70を追加した
- **data/parsed/20240526_run72.csv**: Run72を追加した
- **data/parsed/20240526_run73.csv**: Run73を追加した
- **haniwers/config.py**: tomliに置き換えた
- **haniwers/config.py**: UserSettingsクラスを追加した
- **haniwers/daq.py:_read_serial_data_as_list**: 削除予告を追記した
- **haniwers/daq.py:RealEvent**: 文字列に変換するメソッドを追加した
- **haniwers/daq.py:save_serial_data**: 削除予定のメッセージを追記した
- **haniwers/dataset.py:Run**: threshold_logsを追加した
- **haniwers/mimic.py:FakeEvent**: daq.RealEventから継承することにした
- **haniwers/threshold.py:scan_threshod_by_channel**: ファイル番号にスレッショルド値を追加した
- **haniwers/threshold.py:scan_thresholds_in_parallel**: try文を修正した

### Refactor

- **haniwers/daq.py:init_parser**: ゾンビみたいに復活していたので削除した
- **haniwers/mimic.py**: F401 (unused import) を修正した
- **notebooks/50_config.ipynb**: ファイル名を変更した

### Notebooks

- **mystmd/logbook/run50/20240507_run50.ipynb**: Run68を追加した
- **mystmd/logbook/run50/20240507_run50.ipynb**: Run70 - Run73を追加した
- **notebooks/00_config.ipynb**: UserSettingsクラスをBaseModelに書き換えた
- **notebooks/50_config_daq.ipynb**: config.Daqクラスの動作確認を追加した
- **notebooks/50_config_rundata.ipynb**: RunDataクラスの動作を確認するノートブックを追加した
- **notebooks/50_config_usersettings.ipynb**: UserSettingsクラスを作った
- **notebooks/50_config.ipynb**: configモジュールの確認ができるようにした
- **notebooks/51_daq_main_daq.ipynb**: シリアル通信のテスト中
- **notebooks/51_daq_realevent.ipynb**: RealEventオブジェクトをデータフレームに変換できることを確認した
- **notebooks/51_daq_realevent.ipynb**: RealEventクラスのテストを追加した
- **notebooks/55_threshold_scan_threshold_in_paralell.ipynb**: 機能テスト用のノートブックを追加した
- **notebooks/55_threshold_scan_threshold_in_paralell.ipynb**: 確認中
- **sandbox/hnw.toml**: 設定ファイルを更新した

## 0.14.4 (2024-05-25)

スレッショルド測定のときに、ファイル名にチャンネル番号と設定値が残るようにしました。

### Fix

- **haniwers/daq.py:get_savef_with_timestamp**: ファイル識別子を7ケタに変更した
- **haniwers/dataset.py:PathSettings**: パスがないときの処理を追加した
- **haniwers/dataset.py:Run**: Runクラスを移動した
- **haniwers/dataset.py**: PathSettingsクラスを移動した
- **haniwers/threshold.py:scan_threshold_by_channel**: ファイル名にチャンネル番号と設定値が残るようにした

### Refactor

- **haniwers/daq.py:get_savef_with_timestamp**: 引数名をfidに変更した
- **haniwers/dataset.py**: deprecationメッセージを追加した

### Notebooks

- **mystmd/logbook/run32/20230819_run32_kamiokalab.ipynb**: Run32の解析を整理した
- **mystmd/logbook/run34/20230822_run34_airplane.ipynb**: Run34の解析を整理した
- **mystmd/logbook/run50/20240507_run50.ipynb**: データの読み込みを dataset.Runオブジェクトに変更した
- **mystmd/logbook/run65/20240520_run65.ipynb**: PathSettingsクラスを修正した

### Datasets

- **data/parsed/20230827_run40.csv**: Run40を追加した

## 0.14.3 (2024-05-24)

PyPIにパッケージを公開し、pipでインストールできるようになりました。

以前から使わなくなっていた間数などを削除しました。

### Fix

- **haniwers/mimic.py:FakeEvent**: is_fake=Trueに反転した
- **haniwers/preprocess.py:resample_data_loop**: resample_data_with_hit_typeに変更した
- **notebooks/04_ondotori.ipynb**: すべてのデータを読み込むことにした
- **notebooks/ondotori.ipynb**: うっかり実行しないようにコメントアウトした
- **pyproject.toml**: matplotlibを追加した
- **pyproject.toml**: PyPI公開のためにメタ情報を追加した
- **README.md**: PyPI公開用にREADMEを更新した
- **sandbox/config.toml**: Run64を追加した
- **sandbox/config.toml**: Run65を追加した
- **sandbox/config.toml**: Run66を追加した
- **sandbox/config.toml**: Run67を追加した
- **sandbox/config.toml**: 過去の測定データのパスを変更した

### Logbook

- **logbook**: ノートブックの配置を変更した
- **mystmd/logbook/run50/20240507_run50.ipynb**: ランを簡単に追加できるようにした
- **mystmd/logbook/run65/20240520_run65.ipynb**: Run65関連のおんどとりデータを整理した
- **mystmd/README.md**: シンボリックリンクを削除した

### Datasets

- **data/raw_data/ondotori/**: おんどとりの生データをraw_dataに移動した
- **data/test_data/ondotori**: 昨年のおんどとりのデータを整理した
- **data/test_data/ondotori/TR41**: (deleted) 不要なおんどとりのデータを削除した

### Refactor

- **haniwers/cli.py:raw2csv**: (deprecated) raw2csv を削除した
- **haniwers/daq.py:__main__**: __main__を削除した
- **haniwers/daq.py:daq**: (deprecated) daqを削除した
- **haniwers/daq.py:init_parser**: (deprecated) init_parser を削除した
- **haniwers/daq.py**: icecreamモジュールは使ってない
- **haniwers/raw2csv.py: parser**: (deprecated) parserを削除した
- **haniwers/raw2csv.py:parse_data**: (deprecated) parse_dataを削除した
- **haniwers/raw2csv.py:parse_files**: (deprecated) parse_filesを削除した
- **haniwers/raw2csv.py:resample_data**: (deprecated) resample_dataを削除した
- **haniwers/raw2csv.py**: (deleted) raw2csv.pyを削除した

## 0.14.2 (2024-05-20)

Run60、Run62のデータを追加して、Run50関連の長期測定の解析を更新しました。

また、レイアウトが崩れていたAPIドキュメントの一部も更新しました。


### Fix

- **haniwers/postprocess.py**: 結果をグラフにするモジュールを作成した
- **mystmd/logbook/20240507/run50.ipynb**: 気圧、湿度の二軸グラフを追加した
- **mystmd/tutorials/03-search_port.ipynb**: ポートを検索する方法を追加した
- **sandbox/config.toml**: Run60を追加した
- **sandbox/config.toml**: Run62を追加した

## 0.14.1 (2024-05-18)

未整理だったランの情報を整理しました。
合わせて、前処理したCSVファイルはLFSで管理することにしました。
これまでに追加したファイルもLFS管理に変換しました。

Run50からはじまる長期測定のデータ解析も追加しました。

### Fix

- **.gitattributes**: CSVをLFSに追加した
- **haniwers/cli.py:loguru.logger**: loguruのファイル出力のオプションを変更した
- **haniwers/cli.py:run2csv**: ファイルを保存しないときのメッセージを修正した
- **mystmd/logbook/20230809/run1_kofun.ipynb**: Run1の解析を整理した
- **mystmd/logbook/20230809/run20_kofun.ipynb**: Run20の解析を整理した
- **mystmd/logbook/20230904/run21_kofun.ipynb**: Run21の再解析を整理した
- **mystmd/logbook/20240507/run50.ipynb**: Run50関連のデータを解析した
- **mystmd/tutorials/**: 解析チュートリアルの作成を開始した
- **notebooks/10_quick_threshold_fit.ipynb**: スレッショルド測定のラン番号を整理した
- **sandbox/config.toml**: Run48の情報を追加した
- **sandbox/config.toml**: Run49の情報を追加した
- **sandbox/config.toml**: Run50の情報を追加した
- **sandbox/config.toml**: Run51の情報を追加した
- **sandbox/config.toml**: Run52の情報を追加した
- **sandbox/config.toml**: Run53の情報を追加した
- **sandbox/config.toml**: Run55の情報を追加した
- **sandbox/config.toml**: Run57の情報を追加した
- **sandbox/config.toml**: Run58の情報を追加した
- **sandbox/config.toml**: 関連するランを追加した

### Build

- **poetry.toml**: poetry.tomlを追加した
- **pyproject.toml**: hvplotを追加した
- **pyproject.toml**: matplotlibを追加した
- **pyproject.toml**: updated mystmd: 1.2.0 -> 1.2.3

## 0.14.0 (2024-05-16)

スレッショルド測定のファイル出力に気温などを残すようにしました。
これで、気温とスレッショルド値の関係を調べやすくなります。

また、以前から整理したいと思っていたスレッショルドを操作する系の関数を
``daq.py``から``threshold.py``モジュールに移動しました。

### Feat

- **haniwers/daq.py:RealEvent**: 実イベント用のクラスを追加した
- **haniwers/daq.py:scan_ch_vth**: 気温の平均値なども記録できるようにした
- **haniwers/daq.py:time_daq**: 返り値をpd.DataFrameに変更した
- **haniwers/threshold.py:scan_thresholds_in_serial**: 関数名を追加した

### Fix


- **haniwers/cli.py:fit**: スレッショルドをフィットできるようにした
- **haniwers/cli.py**: 保存先ディレクトリの取得メソッドを変更した
- **haniwers/daq.py:scan_ch_vth**: データフレームを利用してファイルを保存することにした
- **haniwers/daq.py:scan_ch_vth**: 廃止予定のメッセージを追加した
- **haniwers/daq.py**: ロガーのメッセージを修正した
- **haniwers/threshold.py:scan_threshold_by_channel**: 関数を追加した
- **haniwers/threshold.py:scan_thresholds_in_parallel**: 関数のホルダーを追加した
- **haniwers/threshold.py:scan_thresholds**: returnを忘れていたので追加した
- **haniwers/threshold.py:scan_thresholds**: 関数名を変更した
- **haniwers/threshold.py:scan**: 関数を移動した: daq.scan_ch_thresholds -> threshold.scan
- **haniwers/threshold.py**: daq.scan_ch_vth を threshold.scan_threshold_by_channelに移動した
- **notebooks/10_quick_dataframe.ipynb**: ノートブックを更新した
- **notebooks/10_quick_threshold_fit.ipynb**: Run56のスレッショルド測定を追加し、新しい形式でもフィットできることを確認した

## 0.13.2 (2024-05-14)

測定ファイルの名前に、測定時刻を追加しました。
同じ日に測定を再開した場合、既存のファイルに追記するようにしていましたが、
これからは別のファイルに保存するようにしました。
（測定を再開した場合、通し番号はリセットされ、また0から開始します）

### Fix

- **haniwers/daq.py:get_savef_with_timestamp**: get_savef_with_timestampを追加した
- **notebooks/51_daq_get_savef.ipynb**: get_savefをテストするノートブックを追加した
- **notebooks/51_daq_get_savef.ipynb**: モジュールの動作を確認した
- **notebooks/60_mimic.ipynb**: tqdmを使って確認した

## 0.13.1 (2024-05-13)

ファイルを保存するときのメッセージにデータ数も表示することにした

### Fix

- **data/test_data/20240422_run48/**: テスト用のデータを追加した
- **haniwers/cli.py:raw2tmp**: 保存するファイルに含まれるデータ数を表示することにした
- **haniwers/cli.py:run2csv**: 保存するファイルに含まれるデータ数を表示することにした

## 0.13.0 (2024-05-12)

擬似イベントを生成するためのモジュールを追加しました

### Feat

- **haniwers/mimic.py**: 擬似イベントを生成するモジュールを追加した

### Fix

- **notebooks/60_mimic.ipynb**: 擬似イベントの生成に必要な間数を追加した
- **notebooks/60_mimic.ipynb**: mimicモジュールの動作確認を追加した

## 0.12.3 (2024-05-08)

``haniwers version --env`` で実行環境を確認できるようにしました

### Fix

- **haniwers/cli.py:version**: envオプションを追加した

## 0.12.2 (2024-05-08)

ログを保存するファイル名を固定しました

### Fix

- **examples/hnw.toml**: スレッショルド測定でチャンネルごとに範囲を設定できるようにした
- **haniwers/cli.py:loguru**: グローバルなロガーのファイル名から時刻を削除した

## 0.12.1 (2024-05-06)

主にクイックチェック用のノートブックを整理しました

### Fix

- **haniwers/daq.py:run**: 例外が発生したときのログメッセージを追加した
- **notebooks/10_quick_dataframe.ipynb**: ノートブック名を変更した
- **notebooks/10_quick_threshold_fit.ipynb**: ノートブック名を変更した
- **notebooks/11_quick_check_kohun.ipynb**: ノートブック名を変更した
- **notebooks/61_preprocess_fit_threshold.ipynb**: スレッショルド測定の結果を図示できるようにした

## 0.12.0 (2024-05-03)

### Feat

- **haniwers/config.py:UserSettings**: UserSettingsクラスを追加した
- **sandbox/hnw.toml**: loguruに必要な設定を見直した
- **sandbox/hnw.toml**: 新しい形式の設定ファイルを追加した
- **examples/**: 設定ファイルの例をまとめるフォルダを作成した
- **examples/daq.toml**: デフォルト値をRPiに合わせた
- **examples/hnw.toml**: rulesを追加した
- **docs/conf.py**: autodocs2のレンダラーをmystに変更した

### Fix

- **haniwers/config.py:UserSettings**: 各種設定を取得する関数名を分かりやすくした
- **notebooks/00_config.ipynb**: BaseModelをベースにしたクラスのサンプルを追加した
- **notebooks/00_config.ipynb**: UserSettingsクラスをテスト中
- **notebooks/00_config.ipynb**: 無理してpydanticを使わないことにした
- **notebooks/10_quick_check.ipynb**: 処理データを更新した
- **pyproject.toml**: pydanticを追加した

## 0.11.1 (2024-05-03)

### Fix

- **.gitignore**: docs/apidocs を除外した
- **docs/conf.py**: sphinx-autodoc2の初期設定を追加した
- **docs/conf.py**: sphinx.ext.autodocs を設定から削除した
- **docs/modules/**: sphinx-autodocに必要だったページを削除した
- **pyproject.toml**: sphinx-autodoc2を追加した

## 0.11.0 (2024-05-02)

### Feat

- **haniwers/cli.py:loguru.logger**: JSON形式でログを保存するようにようにした

### Fix

- **haniwers/cli.py:loguru.logger**: retentionを追加し、30 days キープするようにした
- **haniwers/cli.py:loguru.logger**: rotationを追加し、サイズが50MBを超えたらローテートするようにした

## 0.10.6 (2024-05-02)

### Fix

- **haniwers/cli.py:ports**: returnする場所を間違えていたので修正した
- **haniwers/cli.py:ports**: デバッグ表示を修正した
- **haniwers/cli.py:ports**: ポート情報のデバッグ表示を追記した
- **notebooks/10_quick_check.ipynb**: 確認用のノートブックを更新した

## 0.10.5 (2024-04-22)

### Fix

- **haniwers/cli.py**: 個別のスレッショルド設定ができるようにした

### Docs

- **docs/usage/overview.md**: OSECHIを使った測定の流れを追加した

## 0.10.4 (2024-03-31)

### Fix

- **poetry.lock**: 依存パッケージを更新した
- **pyproject.toml**: juyterlab -> ipykernelだけに変更した
- **pyproject.toml**: pyarrowを更新した
- **pyproject.toml**: typerパッケージを更新した
- **pyproject.toml**: パッケージを更新した

### Document

- **docs/setup/kurikintons.md**: 準備物を追記した
- **docs/setup/osechi.md**: OSECHIの写真を挿入した

## 0.10.3 (2024-02-19)

### Fix

- **haniwers/cli.py**: チャンネル番号の制限範囲のバグを修正した
- **sandbox/daq.toml**: デフォルトの設定値をLinux系にした
- **sandbox/scan.toml**: デフォルトの設定値をLinux系にした

## 0.10.2 (2024-02-11)

### Fix

- **haniwers/cli.py:fit**: 2種類のファイルに書き出すことにした
- **haniwers/cli.py:fit**: フィット結果にタイムスタンプを追加した
- **haniwers/cli.py:scan**: stepのデフォルト値を1に変更した
- **haniwers/cli.py:scan**: vmin/vmaxのオプションを追加した
- **haniwers/cli.py:scan**: チャンネル番号をオプション引数にした
- **haniwers/cli.py:vth**: CLIにint型への変換を追加した
- **haniwers/cli.py:vth**: オプション引数の条件も確認した
- **haniwers/cli.py:vth**: ファイルから閾値の初期値を読み込むようにした
- **haniwers/threshold.py**: 時刻を保存するタイミングを変更した
- **sandbox/scan.toml**: 重複していた設定を削除した

## 0.10.1 (2024-01-16)

### Fix

- **pyproject.toml**: Typerを再度追加した
- **pyproject.toml**: Typerを一時的に削除した

## 0.10.0 (2024-01-16)

### Feat

- **haniwers/threshold.py**: スレッショルド用のモジュールを追加した

### Fix

- **.gitignore**: Nodeを除外した
- **haniwers/cli.py**: 3sigmaの値を出力できるようにした
- **haniwers/preprocess.py**: fit_threshold_by_channelに関数名を変更した
- **haniwers/preprocess.py**: fit_thresholdのデバッグ表示をOFFにした
- **haniwers/preprocess.py**: fit_thresholdの返り値をpd.DataFrameに変更した
- **mystmd/logbook/**: ログブックを移行した
- **mystmd/logbook/index.md**: ログブックのトップを移行した
- **mystmd/README.md**: Mystドキュメントの表紙を追加した

## 0.9.0 (2024-01-13)

### Feat

- **haniwers/preprocess.py**: fit: 閾値を計算する機能を追加した
- **haniwers/cli.py:fit**: 閾値を計算するコマンドを追加した

### Fix

- **haniwers/cli.py:fit**: チャンネル番号が範囲外の時の処理を追加した
- **haniwers/cli.py:fit**: ファイルが見つからないときの処理を追加した
- **haniwers/cli.py:fit**: 閾値をフィットする関数を実装した
- **haniwers/preprocess.py**: 閾値をフィットできるようにした
- **haniwers/preprocess.py**: 関数名を変更した: fit -> fit_threshold
- **pyproject.toml**: pysenの設定を削除した
- 🐛 スキャン時の設定を修正した

### Lognotes

- **docs/logbook/20230809/run20_kofun.ipynb**: 8月の古墳測定におんどとりのデータも追加した
- **docs/logbook/20230809/run20_kofun.ipynb**: 8月の古墳測定のノートを更新した


## 0.8.1 (2023-08-30)

### Fix

- **arduino/kurikintons_x/kurikintons_x.ino**: テスト用のkurikintonsを追加した
- **haniwers/daq.py**: read_serial_data: タイムアウトしたときロガーに表示することにした
- **haniwers/daq.py**: save_serial_data: データの書き込み時にflushすることにした
- **haniwers/raw2csv.py**: deprecated

### Lognotes

- **docs/logbook/20230809/run1_kofun.ipynb**: Run1を整理した
- **docs/logbook/20230809/run20_kofun.ipynb**: Run20を整理した
- **docs/logbook/20230809/run20_kofun.ipynb**: Run20を整理した
- **docs/logbook/20230809/run20_kofun.ipynb**: データのパスを修正した
- **docs/logbook/20230809/run20_kofun.ipynb**: 少し更新した
- **docs/logbook/20230815/run24_threshold_scan.ipynb**: Run24を整理した
- **docs/logbook/20230816/run26_noise_measurement.ipynb**: Run26を整理した
- **docs/logbook/20230817/run28_noise_temperature.ipynb**: 2023年8月17日の測定を整理した
- **docs/logbook/20230819/run32_kamiokalab.ipynb**: カミオカラボの測定を追加した
- **docs/logbook/20230819/run32_kamiokalab.ipynb**: タイトルに作成日を追加した
- **docs/logbook/20230822/run34_airplane.ipynb**: 説明を追加した
- **docs/logbook/20230825/run35_threshold_scan.ipynb**: Run35を整理した
- **docs/logbook/20230825/run35_threshold_scan.ipynb**: Run39を追加した
- **lognotes/20230809/run20_kofun.ipynb**: 8月9日の古墳測定を整理した
- **lognotes/20230817/run28_noise_temperature.ipynb**: ノイズと気温の関係を整理した
- **lognotes/20230819/run32_kamiokalab.ipynb**: hit_typeごとのイベント数の図を追加した
- **lognotes/20230819/run32_kamiokalab.ipynb**: カミオカラボの測定を整理した
- **lognotes/20230822/run34_airplane.ipynb**: 飛行機測定の解析ノートを追加した

### Configurations

- **sandbox/config.toml**: rundata: Run28を追加した
- **sandbox/config.toml**: rundata: Run29を追加した
- **sandbox/config.toml**: rundata: Run30, Run31を追加した
- **sandbox/config.toml**: rundata: Run32を追加した
- **sandbox/config.toml**: rundata: Run33, Run34（飛行機測定）を追加した
- **sandbox/config.toml**: rundata: Run35, Run36を追加した
- **sandbox/config.toml**: rundata: Run37 - 40を追加した
- **sandbox/config.toml**: rundata: 関連するラン番号を追加した
- **sandbox/daq.toml**: デフォルトのファイル数を変更した

### Notebooks

- **notebooks/01_raw2csv.ipynb**: 各関数の実行時間を確認した
- **notebooks/02_raw2csv.ipynb**: polarsへの変換をテスト中
- **notebooks/10_quick_check.ipynb**: 更新した

### Packages

- **pyproject.toml**: jupyterlab_mystを追加した
- **pyproject.toml**: myst-nb 0.18.0（相当）を追加した

### Git

- **.gitignore**: 整理した
- **sandbox/.gitignore**: .datを除外した
- **sandbox/.gitignore**: データ取得中のCSVファイルを除外した

## 0.8.0 (2023-08-17)

### Feat

- **haniwers/preprocess.py**: read_data_with_polars: データの読み込みをpolarsでできるようにした
- **haniwers/preprocess.py**: read_data: 前処理時のデータ読み込みをpolarsに切り替えた

### Notebooks

- **notebooks/01_raw2csv.ipynb**: 動くように修正した
- **notebooks/02_raw2csv.ipynb**: read_with_polarsの動作を確認した
- **notebooks/02_raw2csv.ipynb**: 前処理の一部をPolarsで置き換えて高速化したい
- **notebooks/03_polars2altair.ipynb**: Polarsを使って試行錯誤した

## 0.7.1 (2023-08-17)

### Fix

- **data/parsed**: Run 17, 24, 25, 26を再プロセスした
- **data/parsed**: 前処理した古墳測定のデータを追加した
- **data/parsed/**: 前処理したテスト測定のデータを追加した
- **data/parsed/**: 前処理後の.csv.gzはサイズが大きのでGit管理しないことにする
- **haniwers/cli.py**: scan: vmaxを修正した
- **haniwers/cli.py**: vth: つねにappend=Trueにした
- **haniwers/cli.py**: vth: 測定日のディレクトリに保存することにした
- **haniwers/config.py**: Daq: fname_vth -> fname_logs に変更した
- **haniwers/config.py**: Daq: fname_vth, fname_scan を追加した
- **haniwers/daq.py**: scan_ch_vth: scanした値にdurationを追加し、保存するデータの順番を変更した
- **haniwers/daq.py**: set_vth: 保存先のディレクトリ確認を追加した
- **haniwers/daq.py**: set_vth: 返り値を修正した
- **haniwers/daq.py**: time_daq: 経過時間のデバッグ表示を追加した
- **haniwers/preprocess.py**: resample_data: 測定開始からの経過時間（秒）を追加した
- **haniwers/preprocess.py**: run2csv: read_fromに変更した影響を修正した
- **sandbox/config.toml**: rundata: ランを追加した

### Lognotes

- **lognotes/20230809/run1_kofun.ipynb**: 2022年4月の古墳測定のデータを再整理した
- **lognotes/20230809/run20_kofun.ipynb**: 2023年8月9日の古墳測定の結果を整理した
- **lognotes/20230815/run24_threshold_scan.ipynb**: スレッショルド値を決め方を整理した
- **lognotes/20230816/run26_noise_measurement.ipynb**: スレッショルド値とノイズの関係を整理した

### Notebooks

- **notebooks/03_polars2altair.ipynb**: polarsとaltairでプロットできるかを確認中
- **notebooks/20_physics_20220425.ipynb**: ログノートに移動した
- **notebooks/20_physics_20230809.ipynb**: ログノートに移動した
- **notebooks/20230816_run26.ipynb**: ログノートに移動した
- **notebooks/95_scan_vth.ipynb**: ログノートに移動した

### Packages

- **pyproject.toml**: pyarrowを追加した
- **pyproject.toml**: Pythonのバージョンを修正した（SciPyの追加に必要だった）
- **pyproject.toml**: scipyを追加した

## 0.7.0 (2023-08-14)

### Feat

- **haniwers/cli.py**: scan: コマンドを追加した
- **haniwers/daq.py**: main_daq: DAQ部分を関数にした
- **haniwers/daq.py**: save_serial_data: 取得したデータを保存する部分を関数にした
- **haniwers/daq.py**: scan_ch_vth: スレッショルド値をスキャンする関数を追加した
- **haniwers/daq.py**: time_daq: 測定時間を指定してDAQを実行できるようにした

### Fix

- **.vscode/arduino.json**: VS Codeプラグインの設定を追加した
- **haniwers/config.py**: Daq: overwrite -> append に変更した
- **haniwers/config.py**: get_run: リファクターした
- **haniwers/config.py**: RunData: srcd -> read_from に変換した
- **haniwers/config.py**: RunData: メンバー変数parseを削除した
- **haniwers/config.py**: 設定ファイルを読み込んだときのデバッグ表示をOFFにした
- **haniwers/daq.py**: deprecatedデコレータを追加した
- **haniwers/daq.py**: main_daq: save_serial_dataで置き換えた
- **haniwers/daq.py**: open_serial_connection: シリアル通信を開始する部分を関数にした
- **haniwers/daq.py**: read_serial_data: 空白でsplitしてリストを返すことにした
- **haniwers/daq.py**: run_daq: main_daqから関数名を変更した
- **haniwers/daq.py**: set_vth_retry: スレッショルド設定の成功／失敗を返すようにした
- **haniwers/daq.py**: time_daq: 時間指定で測定できるようにした
- **haniwers/daq.py**: write_vth: スレッショルドを書き込む部分を関数にした
- **haniwers/daq.py**: write_vth: ログを保存する部分にcsvモジュールを使うことにした
- **sandbox/config.toml**: 2023年8月9日の古墳測定の query を追加した
- **sandbox/config.toml**: rundata から parse を削除した
- **sandbox/daq.toml**: timeoutを100秒にした
- **sandbox/scan.toml**: scan用の設定ファイルを追加した

### Notebooks

- **notebooks/10_quick_check.ipynb**: クイックチェックのノートを更新した
- **notebooks/51_main_daq.ipynb**: main_daqを確認するノートを追加した
- **notebooks/52_time_daq.ipynb**: time_daqを確認するノートを追加した
- **notebooks/95_scan_vth.ipynb**: scanを確認するノートを追加した

### Packages

- **pyproject.toml**: deprecatedを追加した

## 0.6.0 (2023-08-13)

### Feat

- **haniwers/cli.py**: vth: すべてのチャンネルに同じスレッショルドを設定できる機能を追加した
- **haniwers/daq.py**: set_vth_retry: スレッショルド書き込みが成功するまで繰り返す機能を追加した
- **haniwers/daq.py**: スレッショルド設定のログを残す機能を追加した
- **notebooks/11_quick_check.ipynb**: 2023-08-09の古墳測定の解析を追加した

### Fix

- **haniwers/cli.py**: vth: スレッショルド設定コマンドを改善した
- **haniwers/cli.py**: vth: 引数vthとchの順番を変更した
- **haniwers/cli.py**: vth: 引数名を trys -> repeat に変更した
- **haniwers/cli.py**: 内部変数の名前を変更した
- **haniwers/daq.py**: set_vth: 成功したときはlogger.successで表示する
- **haniwers/daq.py**: set_vth: 成功した条件を追加した
- **haniwers/daq.py**: set_vth: 関数をリファクターした
- **haniwers/daq.py**: 引数の名前を args -> daq に変更した
- **haniwers/preprocess.py**: add_time: tz-awareな変換を追加した
- **haniwers/preprocess.py**: resample_data: レートのカラム名を変更した
- **notebooks/10_quick_check.ipynb**: ノートブックを更新した
- **notebooks/90_set_vth.ipynb**: スレッショルド設定を確認するノートブックを追加した
- **notebooks/90_set_vth.ipynb**: ノートブックを更新した
- **notebooks/91_vth.ipynb**: daqモジュールの動作確認用のノートブックを作成した
- **notebooks/93_check_vth.ipynb**: スレッショルド設定のログを確認できるようにした
- **sandbox/daq.toml**: timeoutの時間を5秒にした
- **sandbox/daq.toml**: ポートのデフォルトをLinuxに合わせた

## 0.5.0 (2023-08-11)

### Feat

- **haniwers/cli.py**: vth: 値が設定できるまで書き込みを繰り返すようにした
- **haniwers/config.py**: RunData: datetime_offsetを追加した
- **haniwers/preprocess.py**: add_time: データを記録した時刻の修正を追加した
- **haniwers/preprocess.py**: resample_data: レートの計算を追加した
- **haniwers/preprocess.py**: resample_data: 引数をrule:str -> interval:int に変更した
- **sandbox/config.toml**: rundata: 8月9日の古墳測定を追加した

### Fix

- **haniwers/cli.py**: raw2tmp: offsetオプションを追加した
- **haniwers/cli.py**: run2csv: 表示を追加した
- **haniwers/cli.py**: すべてのコマンドに説明を追記した
- **haniwers/daq.py**: set_vth: not in の書き方を修正した
- **haniwers/preprocess.py**: raw2csv : 読み込みファイルがない場合、エラーを表示するようにした
- **haniwers/preprocess.py**: read_data: カラム名を変更した
- **haniwers/preprocess.py**: read_data: データがないときは終了する
- **haniwers/preprocess.py**: read_data: 行がすべてNaNの場合は除外した
- **notebooks/00_config.ipynb**: RunData: datetime_offsetが読み込めることを確認した
- **notebooks/10_quick_check.ipynb**: クイックチェックで作成する図を更新した
- **notebooks/10_quick_check.ipynb**: ノートブックを更新した

## 0.4.0 (2023-08-09)

### Feat

- **haniwers/cli.py**: ports: コマンドを追加した
- **docker/Dockerfile**: docker: イメージを追加した

### Fix

- **haniwers/cli.py**: raw2tmp: 引数を少なくした
- **haniwers/preprocess.py**: read_data: 以前取得した .dat ファイルにも対応した
- **notebooks/10_quick_check.ipynb**: すぐに時間変化を確認できるようにした
- **sandbox/config.toml**: rundata: run 17を追加した
- **sandbox/daq.toml**: max_rowsの値を小さくした

## 0.3.0 (2023-08-07)

### Feat

- **haniwers/cli.py**: daq: コマンドを追加した
- **haniwers/cli.py**: raw2csv: コマンドを実装した
- **haniwers/cli.py**: raw2tmp: コマンドを追加した
- **haniwers/cli.py**: run2csv: コマンドを追加した
- **haniwers/cli.py**: vth: コマンドを追加した
- **haniwers/config.py**: Config.get_run : 関数を追加した
- **haniwers/config.py**: Daq: クラスを追加した
- **haniwers/daq.py**: daq.set_vthを追加した
- **haniwers/preprocess.py**: 前処理するモジュールを書き直した

### Fix

- **data/parsed/20220425_run1.csv.gz**: データが読み込めなかったので変換しなおした
- **haniwers/cli.py**: daq: quietオプションを追加した
- **haniwers/cli.py**: run2csv: RunDataのオプションを確認することにした
- **haniwers/cli.py**: run2csv: saveオプションを追加した
- **haniwers/cli.py**: import: 不要なimportを整理した
- **haniwers/config.py**: Config.get_run: エラーメッセージを追加した
- **haniwers/config.py**: Daq: クラス変数を変更した
- **haniwers/daq.py**: daq.run: 追加した
- **haniwers/daq.py**: ic: loguruで置き換えた
- **haniwers/daq.py**: serial.SerialException: 例外処理を追加した
- **haniwers/daq.py**: verbosity: 使っている部分を置き換えた
- **haniwers/dataset.py**: pd.to_datetime: formatを追加した
- **haniwers/preprocess.py**: run2csv: run.nameを追加した
- **sandbox/config.toml**: 生データのパスを修正した
- **sandbox/daq.toml**: max_rowsを元に戻した

### Notebooks

- **notebooks/00_config.ipynb**: configモジュールの動作確認をした
- **notebooks/01_raw2csv.ipynb**: raw_dataからCSV変換する作業を見直した
- **notebooks/02_raw2csv.ipynb**: pandasをpolarsで置き換えれないかテスト中
- **notebooks/test_config.ipynb**: Daqクラスの動作確認をした

### Git

- **.gitignore**: csv.gzを除外した
- **.gitignore**: csvを除外した
- **.gitignore**: raw_dataを除外した
- **.gitignore**: tmp_ではじまるファイルは除外した

### Packages

- **pyproject.toml**: altair 5.0.1 に更新した
- **pyproject.toml**: FunQに変更した
- **pyproject.toml**: kaleidoを削除した
- **pyproject.toml**: polars 0.18.12 を追加した
- **pyproject.toml**: tqdm 4.65.0 を追加した
- **pyproject.toml**: Typer 0.9.0 を追加した

## 0.2.0 (2022-06-11)

### Fix

- **haniwers/config.py:get_labels**: labels を読み込めるようにした
- **sandbox/config.toml**: 設定ファイルを更新して、すてべ parse=false にした
- **haniwers/config.py:RunData**: load_data にファイルチェックを追加した
- **haniwers/raw2csv.py:parse_data**: イベント条件のカラム名を変更した
- **haniwers/cli.py:main**: logger に elapsed の表示を追加した
- **haniwers/cli.py:main**: added options
- **haniwers/raw2csv.py:parser**: カラムを追加した: runid, name
- **haniwers/config.py**: 設定ファイルに変数がないときも失敗しにくいようにした
- **haniwers/cli.py:main**: version オプションを修正した
- **haniwers/raw2csv.py:run**: 出力先が指定されていない場合は、保存しないようにした
- **haniwers/raw2csv.py**: 使っていない関数を削除した
- **haniwers/raw2csv.py:run**: run の仕様を変更した（引数に RunData をとることにした）
- **sandbox/config.toml**: filename の変数を削除した
- **haniwers/config.py:RunData**: インスタンス変数 filename を削除した
- **haniwers/daq.py**: やっぱり DAQ を追加した
- **haniwers/dataset.py:load_raw_data**: 記録された日付フォーマットの違いを吸収できるようにした
- **haniwers/cli.py:raw2csv**: 設定ファイルの parse を理解できるようにした
- **daq/run3.py**: DAQ は別のリポジトリで管理することにした
- **haniwers/dataset.py**: CSV ファイルも読み込めるようにした

### Feat

- **sandbox/config.toml**: イベント条件を変更した
- **haniwers/config.py:load_data**: RunData オブジェクトが該当するデータを読み込めるようにした
- **haniwers/raw2csv.py:parser**: 関数名を run から parser に変更した
- **haniwers/config.py:RunData**: クラス変数に query を追加した
- **haniwers/plot.py:hit_adc**: プロットを作成するモジュールを追加した
- **haniwers/config.py:RunData**: raw2gz と raw2csv のインスタンス変数を追加した
- **haniwers/dataset.py:load_raw_data**: ヒットパターン（hit_type）を追加した
- **haniwers/cli.py**: ic を loguru.logger で置き換えた

### Log Notes

- **lognotes/20220503/data3_time_hit.png**: まとめ画像を追加した
- **lognotes/20220503/20220503.md**: ログノート名を変更した
- **lognotes/20220610/20220610.md**: ログノートを更新した
- **lognotes/20220610/20220610.md**: ログノートのまとめを追加した
- **lognotes/20220531/20220531.md**: DAQ の変更点を整理した

### Notebooks

- **notebooks/hittype.ipynb**: ノートブック名を変更した
- **notebooks/test_config.ipynb**: 設定を追加した
- **notebooks/time_hit.ipynb**: ヒットの時間変化を確認した
- **notebooks/test_config.ipynb**: config モジュールのテスト
- **notebooks/top_adc.ipynb**: ノートブックを更新した
- **notebooks/main_analysis.ipynb**: ノートブックたちを整理した
- **notebooks/hit_type.ipynb**: ノートブックを更新した
- **notebooks/test_config.ipynb**: ノートブックを更新した
- **notebooks/hit_type.ipynb**: hit_type を調べるノートブックを追加した
- **notebooks/top_adc.ipynb**: ノートブックを修正した
- **notebooks/test_config.ipynb**: config モジュールの動作確認をするノートブックを追加した
- **notebooks/hit_type.ipynb**: hit_type のヒストグラムを作成した
- **notebooks/hit_type.ipynb**: hit_type を調べるノートブックを追加した
- **notebooks/top_adc.ipynb**: ノートブックを更新した
- **notebooks/load_files.ipynb**: ノートブックを修正した
- **notebooks/top_adc.ipynb**: Run7 のヒストグラムを作成してみた
- **notebooks/top_adc.ipynb**: Run6 の ADC 分布も追加した

### Dataset

- **parsed/20220603_run10.csv.gz**: added .csv.gz
- **parsed/20220603_run9.csv.gz**: added .csv.gz
- **parsed/20220602_run8.csv.gz**: added .csv.gz
- **parsed/20220601_run7.csv.gz**: added .csv.gz
- **parsed/20220531_run6.csv.gz**: added .csv.gz
- **parsed/20220523_run5.csv.gz**: added .csv.gz
- **parsed/20220519_run4.csv.gz**: added .csv.gz
- **parsed/20220518_run3.csv.gz**: added .csv.gz
- **parsed/20220425_run1.csv.gz**: added .csv.gz
- **sandbox/config.toml**: 設定ファイルにランを追加した
- **sandbox/config.toml**: Run7 のデータを追加した
- **parsed/parsed_20220425.csv**: removed parsed/parsed\_\*.csv
- **parsed/parsed_20220523.csv**: Run6 を追加した

## 0.1.1 (2022-06-01)

### Fix

- **lognotes/20220526/20220526.md**: ログノートを微修正した
- **lognotes/20220526/20220526.pdf**: ログノートを PDF に変換した
- **lognotes/20220526/20220526.md**: ログノートを更新した
- **notebooks/top_adc.ipynb**: ノートブックを更新した
- **lognotes/20220531.md**: ログノートを追加した
- **lognotes/20220526/20220526.md**: ログノートを更新した
- **notebooks/overview.ipynb**: 新しいノートブックを追加した
- **parsed/parsed_20220523.csv**: Run5 のデータを追加した
- **notebooks/top_adc.ipynb**: データを読み込む部分を修正した
- **lognotes/20220526/20220526.md**: ログを更新した
- **sandbox/config.toml**: Run5 のデータを追加した
- **daq/run3.py:daq_next**: オプションを追加した
- **daq/run3.py:daq**: ドキュメントを読んで、不要な設定を削除した
- **daq/run3.py:daq_next**: 最低限動くように考えて実装中
- **daq/run3.py:init_parser**: parser を初期化する関数を追加した
- **daq/run3.py:daq_next**: DAQ の改良版を作成する
- **daq/run3.py:daq_test**: append する時のメッセージを修正した
- **daq/run3.py:pargs**: オプション解析を関数として分離した
- **pyproject.toml**: pyserial を追加した
- **pyproject.toml**: pendulum を追加した
- **daq/run3.py:main**: KeyboardInterrupt を受け付けるようにした
- **daq/run3.py:daq_test**: show オプションを追加した
- **daq/run3.py:daq_test**: sleep をそれっぽい間隔で追加した
- **daq/run3.py:pseudo_data**: テスト用データを生成する関数を作成した
- **daq/run3.py:daq_test**: DAQ テスト用の関数を作ってみた
- **pyproject.toml**: CLI を変更した
- **haniwers/raw2csv.py**: 使ってない main 関数を削除した
- **haniwers/cli.py**: 困難を分割した
- **haniwers/config.py:get_rules**: 型ヒントを追加した
- **pyproject.toml**: CLI を変更した
- **daq/run3.py**: reformat with pysen
- **haniwers/raw2csv.py:cli**: haniwers/cli.py:raw2csv に移し替えた
- **haniwers/raw2csv.py**: 設定ファイルの読み込みを Config クラスで置き換えた
- **tests/config.toml**: テスト用の設定ファイルを更新した
- **haniwers/config.py**: config モジュールの動作確認できるようにした
- **sandbox/config.toml:rundata**: rawdata -> rundata にセクション名を変更した
- **haniwers/config.py**: RunData クラスを作成した
- **daq/run3.py**: DAQ をいろいろ修正中（これはたぶん動かない）
- **daq/run3.py**: 最初の状態に戻した
- **daq/run3.py**: ファイル名を操作する部分を書き換えた
- **daq/run3.py**: ファイル名操作の改善の途中
- **daq/run3.py**: 出力ファイル名をもっと簡単に作成する
- **daq/run3.py**: reformat with pysen:black
- **daq/run3.py**: シリアル通信する部分をコメントアウトした
- **daq/run3.py**: DAQ を追加した
- **.gitignore**: ignore Icon
- **lognotes/20220526/20220526.md**: ADC 分布の確認のまとめを作成中
- **notebooks/top_adc.ipynb**: ADC 分布の確認中
- **sandbox/config.toml**: すべてのランを解析対象にした
- **notebooks/adc_analysis.ipynb**: 目的が重複していたので削除した
- **notebooks/load_files.ipynb**: config モジュールを使ってデータを読み込めるかテストした
- **sandbox/config.toml**: glob パターンの typo を修正した
- **haniwers/config.py:Rawdata**: 設定ファイルの生データの情報を保持するためのクラスを作成してみた
- **haniwers/dataset.py:test_dataset**: 関数を削除した
- **haniwers/dataset.py:load_files**: ファイルの一覧がない場合は停止するようにした
- **haniwers/config.py:get_fnames**: fixed typo
- **haniwers/config.py:get_fnames**: ランに関係するファイルの一覧を取得できるようにした
- **haniwers/config.py:get_runs**: 設定ファイルに skip がないときは停止するようにした
- **sandbox/config.toml**: skip パラメーターを追加した
- **tests/test_config.py**: Config クラスのテストケースを追加した
- **haniwers/config.py**: Config クラスをいろいろと追加している
- **haniwers/config.py**: Config クラスにいろいろと追加中
- **haniwers/config.py**: 設定ファイルを扱うクラスを dataclass で作成してみてる
- **haniwers/raw2csv.py**: 移動した関数を削除した
- **haniwers/dataset.py:load_files**: 関数を移動した
- **haniwers/dataset.py**: ファイルを扱う関数を共通モジュールとして使えるようにする
- **notebooks/adc_analysis.ipynb**: ADC 分布を調べるためのノートブックを追加した
- **sandbox/config.toml**: KEK 測定（室内）のデータを追加した
- **haniwers/cli.py**: [WIP] click を使ったオプションパーサーをテスト注
- **haniwers/cli.py**: CLI をまとめるためのモジュールを用意した
- **lognotes/20220518.md**: 室内の測定に関するログノートを追加した

## 0.1.0 (2022-05-17)

### Fix

- **notebooks/top_adc.ipynb**: ノートブックを追加した
- **lognotes/20220517.md**: ログノートを追加した
- **lognotes/20220503.md**: ログノートを更新した
- **haniwers/raw2csv.py:load_raw_data**: read_csv するときに t から始める行をスキップするようにした
- **notebooks/main_analysis.ipynb**: ADC 分布のヒストグラムを追加した
- **lognotes/20220503.md**: ログノートを更新した
- **data/.keep**: データ用のフォルダを追加した
- **haniwers/raw2csv.py**: chmod 644
- **haniwers/raw2csv.py**: データ（srcd）のパスのチェックを追加した
- **haniwers/raw2csv.py:cli**: expanduser することにした
- **lognotes/20220503.md**: ログノートを更新した
- **haniwers/raw2csv.py**: データがない場合に、正常に終了するようにした
- **haniwers/raw2csv.py**: ic 出力の prefix を追加した
- **lognotes/20220503.md**: ログノートを更新した
- **ISSUES.md**: やりたいことリストを追加した
- **notebooks/main_analysis.ipynb**: メモを追加した
- **haniwers/raw2csv.py:cli**: 設定ファイルのデフォルトパスを変更した
- **notebooks/plotly.ipynb**: ノートを更新した
- **haniwers/raw2csv.py**: poetry build できるようにディレクトリ構成を変更した
- **notebooks/main_analysis.ipynb**: メイン解析用のノートブックを追加した
- **scripts/raw2csv.py**: ヒット数関係のカラム名を変更した
- **notebooks/config.ipynb**: tomli を使って設定ファイルの読み込みを確認した
- **sandbox/config.toml**: 設定ファイルを独立させた
- **notebooks/histogram.ipynb**: 基本分布を確認するためのノートブックを追加した

### Refactor

- **parsed/parsed_20220427.csv**: CSV ファイルを更新した

### Feat

- **scripts/raw2csv.py:cli**: 設定ファイルを読み込んで実行できるように機能を追加した

## 0.0.5 (2022-05-07)

### Fix

- **notebooks/slowmonitor.ipynb**: 内容を整理した
- **notebooks/total_seconds.ipynb**: 内容を整理した
- **notebooks/raw2csv.ipynb**: 内容が古いので削除した
- **notebooks/parse_files.ipynb**: ほぼ raw2csv の本体
- **notebooks/single.ipynb**: 内容を整理した
- **notebooks/resample.ipynb**: 内容が重複していたので削除した
- **notebooks/load_files.ipynb**: 内容を整理した
- **scripts/raw2csv.py:resample_data**: 測定開始時刻からの経過時間（days / seconds）を追加した
- **scripts/raw2csv.py:resample_data**: レイヤーごとのヒット数を計算できるようにした

### Refactor

- **parsed/parsed\_**: CSV データを更新した

## 0.0.4 (2022-05-04)

### Fix

- **scripts/raw2csv.py:resample_data**: 集計後のカラム名を変更した
- **scripts/raw2csv.py:resample_data**: ヒットなどの累積和も計算することにした
- **scripts/raw2csv.py:cli**: run_id は文字列に変換してデータフレームに追加した
- **scripts/raw2csv.py:resample_data**: 累積和を計算する場所を変更した
- **parsed/parsed\_**: CSV データを更新した
- **scripts/raw2csv.py:cli**: オプション引数に run_id を追加した
- **scripts/raw2csv.py:cli**: スペースなど整理した
- **scripts/raw2csv.py:cli**: 中間ファイルを保存するオプション（--save）を削除した
- **scripts/raw2csv.py**: 不要になった関数などを削除した
- **scripts/raw2csv.py:merge_files**: 関数を削除
- **scripts/raw2csv.py:parse_file**: 関数を削除
- **scripts/raw2csv.py:parse_files**: 同じランのデータをまとめて処理できるようにした
- **scripts/raw2csv.py:resample_data**: インデックスを time にする場所を変更した

### Refactor

- **scripts/raw2csv.py**: 内部で使う変数名を整えた
- **parsed/parsed_20220425.csv**: CSV を更新した

## 0.0.3 (2022-05-04)

### Fix

- **scripts/raw2csv.py:main**: 実行時にサンプリング間隔を表示するようにした
- **scripts/raw2csv.py:parse_data**: 常に resample_data することにした
- **scripts/raw2csv.py:load_files**: 必要なファイルを読み込み、統合したデータフレームを作成できるようにした
- **notebooks/load_files.ipynb**: ファイル読み込みのテストを作成した
- **scripts/raw2csv.py:parse_file**: カラム desc を追加した
- **scripts/raw2csv.py:merge_files**: 関数名を merge_data から merge_files に変更した
- **scripts/raw2csv.py:parse_files**: 関数名を parse_data から parse_files に変更した
- **scripts/raw2csv.py:slice_data**: スライスしたなかで再サンプリング集計も行うことにした
- **scripts/raw2csv.py:resample_data**: ヒットレートを計算するようにした
- **scripts/raw2csv.py:resample_data**: 引数を interval:int に変更した
- **scripts/raw2csv.py:slice_data**: イベントカテゴリ別にデータを分割できるようにした
- **assets/haniwer.png**: アイコンを作成した
- **lognotes/20220503.md**: ログノートを更新した
- **scripts/raw2csv.py**: なんちゃって typehint を追加した
- **pyproject.toml**: plotly を dependencies に追加した
- **pyproject.toml**: プロジェクト名を haniwer に変更した
- **scripts/raw2csv.py:cli**: CLI っぽくした
- **lognotes/20220503.md**: ログノートを更新した
- **lognotes/20220503.md**: まとめノートを更新した
- **lognotes/20220503.md**: Marp でスライドに変換できるように整えた
- **lognotes/20220503.md**: データ解析の一次まとめ用のファイルを作成した
- **parsed/parsed_20220425.csv**: parsed データを更新した
- **.gitignore**: LibreOffice で csv などを開いたときのロックファイルは無視する
- **scripts/raw2csv.py:parse_data**: データ抽出の条件をカラム condition に残すことにした
- **scripts/raw2csv.py:parse_data**: ヒットレートを求めて、カラム hit_rate に保存した
- **scripts/raw2csv.py:parse_data**: カラム interval を追加した
- **scripts/raw2csv.py:parse_data**: hit1 にも query の条件を追加した
- **scripts/raw2csv.py:parse_data**: 引数 freq を interval に変更した
- **scripts/raw2csv.py:parse_data**: 引数 freq を str から int に変更した

### Refactor

- **parsed/parsed_20220427.csv**: 差分チェック用として追加した
- **parsed/parsed_20220425.csv**: 差分チェック用として追加した
- **parsed/v1/**: パース済みデータを v1 として保存した
- **parsed/parsed_20220427.csv**: 一時的に更新した

## 0.0.2 (2022-05-03)

### Fix

- **scripts/raw2csv.py:main**: frequency オプションを秒数指定に変更した
- **notebooks/plotly.ipynb**: px 用ノートブックを更新した
- **scripts/raw2csv.py:merge_data**: docstring を追加した
- **scripts/raw2csv.py:resample_data**: size を計算したときのカラム名を hit に変更した
- **notebooks/plotly.ipynb**: ノートブックを更新した（plotly.express を使ってみる）
- **notebooks/plotly.ipynb**: ノートブックを追加した

## 0.0.1 (2022-05-01)

### Fix

- **pyproject.toml**: ファイル名のタイポを修正した
- **pyproject.toml**: プロジェクト名を決定した
- **parsed/parsed_20220425.csv**: 基本的に読み込めば OK な CSV データを作成した
- **notebooks/draw.ipynb**: ノートブックを更新した
- **scripts/raw2csv.py:merge_data**: データをソートし忘れていた
- **scripts/raw2csv.py:resample_data**: 累積和も計算することにした
- **scripts/config.toml**: （実装予定がないけど）バッチ処理用の設定ファイルを現在のオプションに合わせて修正した
- **scripts/raw2csv.py:parse_data**: 中間ファイルを保存する箇所を修正した
- **scripts/raw2csv.py:load_raw_data**: インデックスを日付オブジェクトに変換するようにした
- **scripts/raw2csv.py:parse_data**: 個別のファイルに保存するためのオプション引数を整理した
- **scripts/raw2csv.py:resample_data**: 実行内容がシンプルになるように整理した
- **notebooks/single.ipynb**: ノートブックを追加した
- **scripts/raw2csv.py:parse_data**: ソートに使うカラムを追加した
- **scripts/raw2csv.py:parse_data**: レイヤー 2 枚の条件（hit2）を追加した
- **scripts/raw2csv.py**: 出力ファイル名をオプションで変更できるようにした
- **scripts/raw2csv.py:merge_data**: すべての処理データを結合したファイルを出力するようにした
- **scripts/raw2csv.py**: 中間ファイルの保存（--save）をオプションにした
- **scripts/raw2csv.py:select_data**: 削除。関数を分離して用意しなくてもよいと判断した
- **scripts/raw2csv.py:get_fnames**: 削除。Path.glob のラッパーだったので不要と判断した
- **scripts/raw2csv.py**: 再サンプリングしたときに adc の合計の計算を追加した
- **.editorconfig**: EditorConfig を追加した
- **scripts/config.toml**: データの説明を記録するためのファイルを追加した
- **scripts/raw2csv.py**: ヘルプ表示を追加した
- **README.md**: パーサーの使い方をメモした
- **.gitignore**: 元データはリポジトリにコミットしない
- **notebooks/raw2csv.ipynb**: ノートブックを追加した
- **scripts/raw2csv.py**: 生データのパーサーを作成した
- **pyproject.toml**: jupyterlab を dev パッケージにとして追加した
- **.gitignore**: ignore maOS and python related files
