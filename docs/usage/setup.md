# 事前準備

## 動作確認した環境

| 機器名 | OS | Debian Codename |
|---|---|---|
| RPi 3B+ | RPi OS (Legacy, 64-bit) | Bullseye |
| RPi 400 | RPi OS (64bit) | Bookworm |

## Python環境を整備する

どのRaspberry Pi環境でも、Python3.11で揃えることにします。
ここでは`uv`を使って揃えてください。

- `Bookworm`の場合

```console
$ python --version
Python 3.11.5

$ apt install pipx
$ pipx ensurepath

$ pipx install uv
$ uv --version
uv 0.43.0

$ uv python install 3.11
```

- `Bullseye`の場合

```console
$ python --version
Python 3.9.2

$ pip install pipx
$ ~/.local/bin/pipx ensurepath

$ pipx install uv
$ uv --version
uv 0.43.0
$ uv python install 3.11
```
