# インストール

2024年に登場し、開発が盛んな`uv`を使ってDAQツール（`haniwers`）をインストールします。

## 測定用のディレクトリを作成する

```console
$ mkdir ~/repos/oseshi-workd/
$ cd ~/repos/osechi-work/
```

## Pythonの実行環境を固定する

```console
$ uv python pin 3.11
==> .python-version
```

## DAQツールをインストールする

```console
$ uv tool install haniwers
$ haniwers version --env
```

## （オプション）をアンインストールする

```console
$ uv tool uninstall haniwers
$ pipx uninstall uv
$ sudo apt remove pipx
```
