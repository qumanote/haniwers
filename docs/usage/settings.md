# 設定ファイル

:todo:
- 以下の手順を `haniwers new パス名` で実行できるようにしたい
- 設定ファイルを `haniwers.toml` に統一したい


## 作業ディレクトリを作成する

```console
$ mkdir ~/repos/sandbox/haniwers-work/
$ cd ~/repos/sandbox/haniwers-work/
(haniwers-work) $ haniwers --help
```

- 作業用のディレクトリを作成してください
- 作業用のディレクトリに移動して``haniwers``コマンドを実行してください

## 設定ファイルを作成（コピペ）する

- 以下の2つの設定ファイルを、作業ディレクトリに作成してください。
- まずはコピペでOKです

### `daq.toml`

```toml
saved = ""
skip = 10
prefix = "osechi_data"
suffix = ".csv"
max_rows = 1000
max_files = 1000
device = "/dev/ttyUSB0"
baudrate = 115200
timeout = 100
fname_logs = "threshold_logs.csv"
fname_scan = "threshold_scan.csv"
```

### `scan.toml`

```toml
saved = ""
skip = 10
prefix = "scan_data"
suffix = ".csv"
max_rows = 1000
max_files = 10
device = "/dev/ttyUSB0"
baudrate = 115200
timeout = 15
fname_logs = "threshold_logs.csv"
fname_scan = "threshold_scan.csv"
```

上記のサンプルは[haniwers/examples](https://gitlab.com/qumasan/haniwers/-/tree/main/examples)からダウンロードできます。

## ログファイル（``haniwers_logs.json``）

``v0.15.1``で、``haniwers``コマンドを実行したときのログを、システム定番のログディレクトリに、10日前までのログを保存するように変更しました。
JSON形式（正しくはJSONL形式）で保存されるため、``jq``コマンドなどでパースして確認することをオススメします。

:macOS: ``~/Library/Logs/haniwers/0.15.1/haniwers_logs.json``
:Linux:（あとで確認する）
:Windows: （使う時があれば確認する）

```console
$ less ~/Library/Logs/haniwers/0.15.1/haniwers_logs.json | jq
```

それ以前のバージョンでは、``haniwers``コマンドを実行したディレクトリの``logs/haniwers_logs.json``に保存されます。
もし不必要なパスに作成されてしまった場合は、手動で削除してしまって構いません。
