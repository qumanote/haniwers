# 開発ノート


```{toctree}
---
maxdepth: 1
---
env
data
columns
ipynb
```

---
## ログブック

- 解析結果などは記録用のファイルを作成し、整理する
- ``VSCode + Marp``を使ってスライドに変換する


## ツールの共同開発

- このリポジトリをクローンして、一緒に開発を進める方法



```bash
$ git clone git@gitlab.com:qumanote/haniwers.git
$ cd haniwers
$ poetry install
$ poetry shell
(venv) $ cd sandbox
(venv) $ haniwers -h
```

1. ``poetry`` : Python開発の仮想環境を構築するコマンド；別途システムへのインストールが必要
1. ``poetry install`` :  このパッケージが依存している外部パッケージをインストールして、開発環境を整える
1. ``poetry shell`` : 仮想環境の中のシェルを起動し、コマンドライン作業を行う

もう少し整理してから``PyPI``パッケージとして公開したい
