# 開発環境

測定と解析をしたことがある環境は以下の通りです。

## データ取得

- `Rasberry Pi OS (64-bit)` + `Python3.9`
- `MacBook Pro (2018 / Intel)` + ``macOS Monterey (12.6)`` + ``Python 3.11``

## データ解析

- `MacBook Pro (2018 / Intel)` + ``macOS Monterey (12.4)`` + ``Python 3.9``
- `Mac mini (2020 / M1)` + ``macOS Big Sur (11.6)`` + ``Python 3.10``
- `MacBook Air (2022 / M2)` + ``macOS Monterey (12.4)`` + ``Python 3.11``
