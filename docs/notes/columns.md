# データのカラム名

## 生データ（``.csv``）のカラム

``localtime``
: イベント時刻です。
  ``YYYY-MM-DDTHH:mm:ss``もしくは``YYYY-MM-DDTHH:mm:SSZ``の形式です。
  使ったDAQのバージョンによって``tz-naive`` / ``tz-aware``になっています。

``top``
: 上段シンチのヒットの有無（``top > 0``）です。
  値の大きさは関係ありません。

``mid``
: 中段シンチのヒットの有無（``mid > 0``）です。
  値の大きさは関係ありません。

``btm``
: 下段シンチのヒットの有無（``btm > 0``）です。
  値の大きさは関係ありません。

``adc``
: 上段シンチのADC値です。
  値は光量に比例します。

``tmp``
: 気温です。``℃``

``atm``
: 気圧です。``Pa``

``hmd``
: 湿度です。``%``

---

## 前処理後のデータ（``.csv.gz``）のカラム

- 生データのカラム
- ``hit_top`` : ``True`` if ``top > 0``
- ``hit_mid`` : ``True`` if ``mid > 0``
- ``hit_btm`` : ``True`` if ``btm > 0``
- ``hit_type`` : ヒットのパターンを8ビットで表現したときの十進数
  - ``hit_top`` * 4 + ``hit_mid`` * 2 + ``hit_btm`` * 1

| ``hit_type`` | ``top`` | ``mid`` | ``btm`` |
|:---:|:---:|:---:|:---:|
| 0 | 0 | 0 | 0 |
| 1 | 0 | 0 | 1 |
| 2 | 0 | 1 | 0 |
| 3 | 0 | 1 | 1 |
| 4 | 1 | 0 | 0 |
| 5 | 1 | 0 | 1 |
| 6 | 1 | 1 | 0 |
| 7 | 1 | 1 | 1 |

## 集計後のデータ（``.csv``）のカラム

- ``time``
- ``events``
- ``hit_type``
- ``hit_top`` / ``hit_mid`` / ``hit_btm``
- ``event_rate``
- ``event_rate_top`` / ``event_rate_mid`` / ``event_rate_btm``
- ``interval``
- ``run_id``
- ``name``
- ``description``
