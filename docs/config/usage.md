# 設定ファイル

- オプション引数で設定できることは、すべて設定ファイルに書いておくことができる
- ``srcd``や``filename``などのパスは設定ファイルからの**相対パス**で指定する

```toml
[rules]
hit1 = "top > 0 or mid > 0 or btm > 0"
hit2 = "top > 0 and btm > 0"
hit3 = "top > 0 and mid > 0 and btm > 0"
```

## 設定できるセクションと項目

```{toctree}
rules
rawdata
```
