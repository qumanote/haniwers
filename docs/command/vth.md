# ``haniwers vth``（スレッショルドを設定する）

```console
$ haniwers vth --help
```

:::{literalinclude} ./help/help-vth
:::

## 基本コマンド

```console
$ haniwers vth
```

カレントディレクトリにある``thresholds_latest.csv``を読み込んで、
スレッショルド値の最良推定値を設定します。

## オプション

1. ``--ch``, ``--vth``
2. ``--max-retry``
3. ``--load-from``

### 個別のチャンネルに設定したい（``--ch``、``--vth``）

```console
$ haniwers vth --ch 1 --vth 278
$ haniwers vth --ch 2 --vth 268
$ haniwers vth --ch 3 --vth 301
```

``--ch``と``--vth``オプションを使って、チャンネル番号を指定して、スレッショルド値を設定できます。
チャンネル番号は``[1, 2, 3]``のいずれか、スレッショルド値は0より大きい値を設定してください。
範囲外の数値を設定した場合は、エラーを表示して終了します。

### リトライ数を増やしたい（``--max-retry``）

```console
$ haniwers vth 300 1 --max-retry 10
```

``--max-retry 回数``オプションで書き込み回数を変更できます。
デフォルトは``3``回です。

:::{note}

原因がよく分かっていないのですが、OSECHIのスレッショルド書き込みが1回で成功しない場合があります。
そのために、書き込みに失敗した場合にリトライできるようにしてあります。
何回リトライしても書き込めない場合は、OSECHIの電源をリセットしてください。

解析時のデータクォリティチェックに利用することを想定して、
このコマンドのログを``測定日/threshold_logs.csv``に出力しています。
ログファイルのカラムは``time, ch, vth, result``です。

:::
