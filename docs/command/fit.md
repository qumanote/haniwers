# ``haniwers fit``（スレッショルドの最適値を推定する）

```console
$ haniwers fit --help
```

:::{literalinclude} ./help/help-fit
:::

## 基本コマンド

```console
$ haniwers fit ディレクトリ名
```

ディレクトリ名をして、スレッショルド測定のデータから、閾値の推奨値を計算します。
推奨値は $1 \sigma$、$3 \sigma$、$5 \sigma$``で計算しています。
適切と思われる値を選択し、利用してください。

チャンネル番号の指定は、デフォルトですべてのチャンネルになっています。
個別のチャンネル番号を指定することもできます。

## オプション

1. ``--ch``
2. ``--search-pattern``

### チャンネルを指定したい（``--ch``）

```console
$ haniwers fit ディレクトリ名 --ch 1    # Ch1のみ
$ haniwers fit ディレクトリ名 --ch 2    # Ch1のみ
$ haniwers fit ディレクトリ名 --ch 3    # Ch1のみ
$ haniwers fit ディレクトリ名 --ch 0    # すべてのチャンネル（デフォルト）
```

``--ch チャンネル番号``オプションで個別のチャンネルだけ指定できます。
デフォルトは``0``（すべてのチャンネル）です。

### ファイル名を指定したい（``--search-pattern``）

```console
$ haniwers fit ディレクトリ名 --search-pattern "*ファイル名"
```

``--search-pattern ファイル名``オプションで、スレッショルド値の計算に使うファイル名を変更できます。
ファイル名の指定は``glob``パターンに対応しています。
デフォルトは``threshold_scan.csv``です。
