# ``haniwers scan``（スレッショルドを測定する）

```console
$ haniwers scan --help
```

:::{literalinclude} ./help/help-scan
:::

## 基本コマンド

```console
$ haniwers scan # すべてのCH
```

``vmin``から``vmax``の範囲で``step``ごとにスレショルド値を変更しながら、
``duration``秒のイベント数を測定してスレッショルド曲線を測定します。

デフォルトで、すべてのチャンネルに対して順番にスキャンします。

スキャンした結果は``測定日/threshold_scan.csv``に保存されます。
カラムは``time, duration, ch, vth, events``です。

:::{note}

デフォルトは``--step 10``に設定していますが、``--step 1``が実用的です。
そのうちデフォルト値を変更することを考えています。

:::

単純に計算すると ``チャンネル数 * スレッショルド値の数 * 1点の測定時間``（秒）の測定時間が必要です。
シリアル通信がうまくいかない場合、最大``チャンネル数 * スレッショルド値の数 * タイムアウト``（秒）かかります。

## オプション

1. ``--ch``
1. ``--step``
2. ``--duration``
3. ``--load-from``
4. ``--quiet``

### チャンネルを個別指定したい

```console
$ haniwers scan --ch 1    # CH1のみ
$ haniwers scan --ch 2    # CH2のみ
$ haniwers scan --ch 3    # CH3のみ
```

``--ch チャンネル番号``オプションでチャンネル番号を個別に指定できます。

### 測定間隔を変更したい（``--step``）

```console
$ haniwers scan --step 5    # すべて
$ haniwers scan --step 5 --ch 1    # CH1のみ
```

``--step ステップ数``オプションでスレッショルド値の測定間隔を変更できます。
デフォルトの測定間隔は``1``です。

### 測定範囲を変更したい（``--vmin`` / ``--vmax``）

```console
$ haniwers scan --vmin 200 --vmax 400 --step 10
```

``--vmin``と``--vmax``オプションで測定範囲を変更できます。
デフォルトは``(250, 311)``です。

### 1点の測定時間を変更したい（``--duration``）

```console
$ haniwers scan --duration=30
```

``--duration 秒数``オプションで1点あたりの測定時間を変更できます。
デフォルトの測定時間は``10``秒です。

### 設定ファイルを変更したい（``--load-from``）

```console
$ haniwers scan --load-from="scan.toml"
```

``--load-from ファイル名``で設定ファイルを変更できます。
デフォルトのファイル名は``scan.toml``です。

設定内容は[daq.toml](./daq.md)と同じです。

:::{note}

特定のチャンネルだけスキャンするために、開始直後にすべてのチャンネルのスレッショルドを高めに設定しています。
（2023/08/15時点で500に設定）。
そのあとで、該当するチャンネルだけに対してスレッショルド値を変更しながら測定します。
そのため、すべてのチャンネルを同時にスキャンすることはできません。
（これは、主にDAQの都合で、原理的にはできるはずです。が、たぶんやらないです）

:::
