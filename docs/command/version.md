# ``haniwers version``（バージョンを確認する）

```console
$ haniwers version
haniwers 0.17.0
```

## Raspberry Piで確認

```console
$ haniwers version --env
haniwers 0.19.2

Environments:

    Logs: ~/Library/Logs/haniwers/0.19.2/haniwers_log.json
    Executable: ~/.local/share/uv/tools/haniwers/bin/python
    Python: 3.11.2
    Implementation: CPython
    Compiler: GCC 12.2.0
    OS: Linux-6.6.31+rpt-rpi-v8-aarch64-with-glibc2.36
    System: linux
    Platform: Linux
    Kernel: 6.6.31+rpt-rpi-v8
    Arch: aarch64
```

## macOSで確認

```console
$ haniwers version --env
haniwers 0.19.2

Environments:

    Logs: ~/Library/Logs/haniwers/0.19.2/haniwers_log.json
    Executable: ~/.local/pipx/venvs/haniwers/bin/python
    Python: 3.12.5
    Implementation: CPython
    Compiler: Clang 15.0.0 (clang-1500.3.9.4)
    OS: macOS-14.6.1-arm64-arm-64bit
    System: darwin
    Platform: Darwin
    Kernel: 23.6.0
    Arch: arm64
```

## Docker Desktop (macOS)で確認（RPi4:Bookworm）

```console
$ haniwers version --env
haniwers 0.19.2

Environments:

  Logs: /root/.local/state/haniwers/0.19.2/log/haniwers_log.json
  Executable: /root/.local/share/uv/tools/haniwers/bin/python
  Python: 3.11.2
  Implementation: CPython
  Compiler: GCC 12.2.0
  OS: Linux-6.10.0-linuxkit-aarch64-with-glibc2.36
  System: linux
  Platform: Linux
  Kernel: 6.10.0-linuxkit
  Arch: aarch64
```

`balenalib/raspberrypi4-64-python:bookworm-build-20240429`のイメージを使って、`uv`でインストールした

## Docker Desktop (macOS) で確認（RPi4:Bullseye）

```console
$ haniwers version --env
haniwers 0.19.2

Environments:

  Logs: /root/.local/state/haniwers/0.19.2/log/haniwers_log.json
  Executable: /root/.local/share/uv/tools/haniwers/bin/python
  Python: 3.11.2
  Implementation: CPython
  Compiler: GCC 10.2.1 20210110
  OS: Linux-6.10.0-linuxkit-aarch64-with-glibc2.31
  System: linux
  Platform: Linux
  Kernel: 6.10.0-linuxkit
  Arch: aarch64
```

`balenalib/raspberrypi4-64-python:bullseye-build-20240429`で確認した

## WSL2（Ubuntu）で確認

```console
$ haniwers version --env
haniwers 0.19.2

Environments:
    Logs: ~/.local/state/haniwers/0.19.2/log/haniwers_log.json
    Executable: ~/.local/pipx/venvs/haniwers/bin/python
    Python: 3.10.12
    Implementation: CPython
    Compiler: GCC 11.4.0
    OS: Linux-5.15.153.1-microsoft-standard-WSL2-x86_64-with_glibc2.35
    System: linux
    Platform: Linux
    Kernel: 5.15.153.1-microsoft-standard-wsl2
    Arch: x86_64
```

WSL2に`haniwers`をインストールできるが、ポート設定の方法がわからず、OSECHIと通信できない
