# ``haniwers raw2tmp``（取得したデータを簡単に確認する）

```console
$ haniwers raw2tmp --help
```

:::{literalinclude} ./help/help-raw2tmp
:::


## 基本コマンド

```console
$ haniwers raw2tmp ./日付
```

測定した生データをすぐに前処理して解析用データに変換します。
処理されたデータは``raw2tmp.csv``に出力されます。

:::{note}

解析用データに変換するときに、ヒット情報やヒットパターンの情報が追加したり、
指定した時間間隔で測定値を再集計したりしています。

解析用データは``Jupyter Lab / Notebooks``で初期解析しています。

:::

## 前処理の内容

``add_time``
: 時刻に関する情報を追加します。
  時間情報を``datetime``オブジェクトに変換します。
  ``tz-naive``な場合は``tz-aware``に変換します。
  測定時刻と実時刻が（大きく）ずれてしまった場合に調整します。

``add_hit``
: 各レイヤーのヒットの有無を判定します。
  ``hit_top`` / ``hit_mid`` / ``hit_btm`` のカラムを追加します。

``add_hit_type``
: イベントのヒットパターンを計算します。
  ``hit_top * 4 + hit_mid * 2 + hit_btm`` で求めた値を``hit_type``に追加します。

``resample_data``
: 指定した時間でデータを再集計します。
