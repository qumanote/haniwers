# リポジトリをクローンする

```console
$ mkdir ~/repos/gitlab.com/
$ git clone https://gitlab.com/qumasan/haniwers.git
$ cd haniwers
```

1. ホームディレクトリに作業用のディレクトリを作成してください。
ここでは`~/repos/gitlab.com/`としています。
2. GitLabリポジトリから`haniwers`をクローンしてください。
