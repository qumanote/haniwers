# 仮想環境をセットアップする

```console
$ cd haniwers
$ poetry config virtualenvs.in-project true
$ poetry shell
(.venv) $
```

1. リポジトリのディレクトリに移動し、仮想環境を構築してください
2. 仮想環境はプロジェクト内に構築するように設定します
3. `poetry shell`で仮想環境を起動してください
