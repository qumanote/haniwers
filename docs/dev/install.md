# 開発パッケージをインストールする

```console
(.venv) $ poetry install
Installing dependencies from lock file
Package operations: 0 installs, 0 updates, 0 removals
Installing the current project: haniwers (0.0.5)

(venv) $ which haniwers
~/repos/haniwers/.venv/bin/haniwers

(venv) $ haniwers --help
```

1. 仮想環境に開発パッケージをインストールしてください
2. インストールしたCLIのパスを確認してください
3. ``haniwers --help``を実行してください
