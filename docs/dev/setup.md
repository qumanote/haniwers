# 事前準備

## 動作確認した環境

| 機器名 | OS | Chip |
|---|---|---|
| MacBook | macOS 13 Ventura | Intel |
| MacBook Air (M2, 2022) | macOS 14.6.1 Sonoma | Apple M2 |

## 開発環境を整備する

`poetry`を使って開発環境を整えます。

- `macOS`の場合

```console
$ brew install git
$ brew install pipx
$ pipx ensurepath
$ pipx install poetry
```
