# Raspberry Piの設定

Raspberry Pi OSの設定については
[Raspberyy Piの使い方 - KumaROOT](https://kumaroot.readthedocs.io/ja/latest/raspi/raspi-usage.html)を参照してください。

## 使用しているモデルを確認する

```console
$ more /proc/device-tree/model
Raspberry Pi 3 Model B Rev 1.2
Raspberry Pi 400 Rev 1.
```

{file}`/proc/device-tree/model`の中身で、使っているRaspberry Piのモデルが確認できます。

## Linuxパッケージを更新する

```console
// パッケージを定期的に更新する
$ sudo apt update
$ sudo apt upgrade

// 最初 もしくは たまに実行する
$ sudo apt full-upgrade
```

Linux関係のパッケージは定期的に更新が必要です。
セキュリティパッチなども含まれるため、こまめに実施するのがよいと思います。
よほどのことがないかぎり、更新を適用して動かなくなることはないと思います。

## Pythonの設定を確認する

```console
// Pythonのバージョンを確認する
$ python3 --version
Python 3.11.2

// pipのバージョンを確認する
$ pip3 --version
pip 23.0.1 from /usr/lib/python3/dist-packages/pip (python 3.11)

// pipを更新する（定期的）
$ pip install -U pip
```

Pythonのバージョンを確認しておきます。
Raspberry Pi 400に使ったRaspberry Pi OS（64-bit / Debian Bookworm）は
``Python3.11``がプリインストールされていました。
Pythonパッケージを管理する``pip``コマンドも（定期的に）更新してください。

## pipxを設定する

```console
// インストールする
$ sudo apt install pipx

// パスを設定する（初回だけ）
$ pipx ensurepath
Success! Added /home/osechi/.local/bin to the PATH environment variable.

// パス設定を確認する
$ echo $PATH
/home/osechi/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/games:/usr/games
```

PyPIにある``haniwers``を使う場合、``pipx``でインストールできます。
まず``apt``を使って``pipx``をインストールしてください。
そして``pipx ensurepath``でパスを設定します。

## （オプション）uvを設定する

```console
// uvをインストールする（オプション）
$ pipx install uv
```

`uv`は`pip`や`pipx`を代替するパッケージ管理コマンドです。
Rust言語で実装されているため、`pip`/`pipx`と比べて高速に動作します。
まだAPTリポジトリに登録されていないため、`pipx`でインストールします。

:::{caution}

`pipx`と`uv tool`でインストールされるバイナリーのパスは同じ（`$HOME/.local/bin`）です。
どちらのツールを使うかは好みですが、まぜないようにしたほうがいいと思います。

:::

## （オプション）poetryを設定する

```console
// poetryをインストールする（オプション）
$ sudo apt install python3-poetry
```

GitLabリポジトリの``haniwers``を使う場合、開発環境の構築が必要です。
``apt``を使って``poetry``をインストールしてください。

## （オプション）SSHの設定

同一のアクセスポイント（無線LANやテザリング）に接続しているパソコンからSSHログインできるようにしておくと便利です。
デフォルトのユーザー名の設定が必須です。
詳細は[KumaROOT](https://kumaroot.readthedocs.io/ja/latest/raspi/raspi-ssh.html)に整理しました。

:::{note}

デフォルトのユーザー名とホスト名は``pi@raspberrypi``でしたが、現在は非推奨です。
同じものを設定することは禁止されていませんが、別のユーザー名とホスト名を設定してください。

:::

## （オプション）VNCの設定

詳細は[KumaROOT](https://kumaroot.readthedocs.io/ja/latest/raspi/raspi-vnc.html)に整理しました。

## （オプション）Dockerの設定

Docker公式のチュートリアルにしたがって、Docker関係のパッケージをインストールしてください。
