# Kurikintonsの設定

OSECHIに書き込むスケッチの名前です。
スケッチの編集は任意のエディター（VS CodeやArduino IDE）でよいですが、スケッチの書き込みにはArduino IDEの環境が必要です。

## 準備するもの

- スケッチをビルド／書き込むためのパソコン
- OSECHI本体
- USBケーブル（パソコンとOSECHIを接続）
- Arduino IDE
- kurikintonsのスケッチ（``.ino``ファイル）

:::{note}

パソコンは自身が普段使っているパソコンがよいと思います。
測定用のRaspberry Piも使えますが、ビルド／書き込みに時間がかかります。

:::

## Arduino IDEをインストールする

``kurikintons``のスケッチをビルド＆書き込むために[Arduino IDE](https://www.arduino.cc/en/software)をインストールします。

macOSの場合はHomebrewでインストールできます。

```console
$ brew install arduino-cli
$ brew install --cask arduino-ide
```

``arduino-cli``もしくは``arduino-ide``をインストールします。
両方インストールしても問題なさそうです。

## IDEを設定する

インストールしたArduino IDEを起動し、設定します。

- ボード : ESP32-WROOM-DA
  - 使っているのは``ESP32-WROOM-32D``です
  - リストになかったので、一番それっぽいものを選択しました
- ポート : リストの中からそれっぽいのを選択してください
- 追加したライブラリ
  - Adafruit BME280 Library by Adafruit
  - Adafruit BusIO by Adafruit
  - Adafruit Unified Sensor by Adafruit

## VS Codeプラグインのインストール

[ArduinoのVS Codeプラグイン](https://marketplace.visualstudio.com/items?itemName=vsciot-vscode.vscode-arduino)もあります。
``arduino-cli``をVS Code上で使うことができます。
``arduino-cli``はプラグインにバンドルされています。

### コマンドパレット

- コマンドパレットを{kbd}`Shift+Command+P`で開き、`arduino`で検索します。
- ``Arduino: Select Serial Port`` : シリアル通信するポートを選択します。
- ``Arduino: Open Serial Monitor`` : シリアル通信の様子を出力モニター（ターミナル）を表示します。
- ``Arduino: Library Manager`` : スケッチのコンパイルに必要なライブラリを管理できます。
  - ESP32関係のライブラリを追加でインストールしました。
  - ライブラリは``~/Library/Arduino15/packages/esp32/...``にインストールされるようです。

## kurikintonsをダウンロードする

``kurikintions``を[haniwersのスケッチ用ディレクトリ](https://gitlab.com/qumasan/haniwers/-/tree/main/arduino)からダウンロードします。
現在の最新バージョンは``kurikintons_v1/kurikintons_v1.ino``です。

:::{note}

自分のテスト用に{file}`arduino/kurikintons_x/kurikintons_x.ino`を作ってあります。
過去のスケッチは{file}`archives/`に保存してあります。

:::

## kurikintonsをビルドする

ダウンロードした``kurikintons``のスケッチファイルを、Arduino IDEで開き、ビルドします。

## kurikintonsを書き込む

パソコンとOSECHIを接続します。
接続したUSBポートを設定して、``kurikintons``を書き込みます。

## Arduino関係のリファレンス

スケッチで使っているヘッダーファイルや関数／メソッドなどのリファレンスです。

- [Serial - Arduino Reference](https://www.arduino.cc/reference/en/language/functions/communication/serial/)
- [SPI - Arduino Reference](https://www.arduino.cc/reference/en/language/functions/communication/spi/)
- [BME280 - Arduino Reference](https://www.arduino.cc/reference/en/libraries/bme280/)
- [Arduino ESP32 - Espressif](https://docs.espressif.com/projects/arduino-esp32/en/latest/getting_started.html)
