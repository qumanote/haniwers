% haniwers documentation master file, created by
% sphinx-quickstart on Sun Jun  5 09:07:09 2022.
% You can adapt this file completely to your liking,
% but it should at least contain the root `toctree` directive.

# Document of haniwers

## User's Guide

Raspberry Piを使った測定手順を紹介します。

```{toctree}
---
maxdepth: 2
---
usage/setup
usage/install
usage/settings
usage/overview
```

## Developer's Guide

```{toctree}
---
maxdepth: 2
---
dev/setup
dev/clone
dev/venv
dev/install
```

## ソフトウェア

```{toctree}
---
maxdepth: 2
---
usage/index
command/index
config/usage
```

## ハードウェア

```{toctree}
---
maxdepth: 2
---
setup/index
```

## 個人ノート

```{toctree}
---
maxdepth: 1
---
notes/index
```

## モジュール一覧

```{toctree}
---
maxdepth: 2
---
apidocs/index
```

# Indices and tables

% - {ref}`genindex`
% - {ref}`modindex`
% - {ref}`search`

# Status

![GitLab Last Commit](https://img.shields.io/gitlab/last-commit/qumasan%2Fhaniwers?style=for-the-badge)

![Gitlab Pipeline Status](https://img.shields.io/gitlab/pipeline-status/qumasan%2Fhaniwers?style=for-the-badge)

![GitLab Tag](https://img.shields.io/gitlab/v/tag/qumasan%2Fhaniwers?sort=semver&style=for-the-badge)

![PyPI - Python Version](https://img.shields.io/pypi/pyversions/haniwers?style=for-the-badge)

![GitLab License](https://img.shields.io/gitlab/license/qumasan%2Fhaniwers?style=for-the-badge)

## Downloads

![PyPI - Downloads](https://img.shields.io/pypi/dd/haniwers?style=for-the-badge)

![PyPI -Downloads](https://img.shields.io/pypi/dw/haniwers?style=for-the-badge)

![PyPI - Downloads](https://img.shields.io/pypi/dm/haniwers?style=for-the-badge)
