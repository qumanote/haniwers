# Release Note

- Version: [e.g., v1.2.0]
- Release Date: [YYYY-MM-DD]

# New Features

1. Feature 1: [Description]
2. Feature 2: [Description]

# Bug Fixes

1. Fix 1: [Description]
2. Fix 2: [Description]

# Enhancements

1. Enhancement 1: [Description]
2. Enhancement 2: [Description]

# Known Issues

1. Issue 1: [Description]
2. Issue 2: [Description]

# Upgrade Notes

[Description of any special steps required to upgrade]
