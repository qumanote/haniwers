# Title: [Brief summary of the bug]

# Description

A clear and concise description of what the bug is.

# Steps to Reproduce

Please provide the steps to reproduce the issue:

1. Go to '...'
2. Click on '...'
3. Scroll down to '...'
4. See the error

# Expected Behavior

Explain what you expected to happen.

# Actual Behavior

Explain what actually happened. If possible, include screenshots or logs.

# Screenshots

If applicable, add screenshots to help explain your problem.

# Environment

- Version: [e.g., v1.0.0]
- OS: [e.g., Windows 10, macOS Big Sur]
- Python: [e.g., Python 3.10]
- Other Details: [Any other environment-specific details]

# Logs and Error Messages

Include any relevant log output or error messages here.

# Additional Context
Add any other context about the problem here.

# Checklist

- [ ] I have checked for similar issues and none match my problem.
- [ ] I have provided sufficient information to reproduce the issue.
- [ ] I have included the version number, OS, and other environment details.
