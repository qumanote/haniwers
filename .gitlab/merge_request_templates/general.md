# Title: [Brief and descriptive title]

# Description

Provide a summary of the changes in this merge request. Include context, motivation, and any relevant details.

# Related Issues

Closes #[issue_number]

# Changes Made

- [ ] [List all changes made in this MR, including new features, bug fixes, etc.]

# How to Test

Describe the steps for testing this merge request. Include any specific setup or test cases needed.

1. Step one
2. Step two
3. Step three

# Screenshots (if applicable)

Attach screenshots or GIFs to help explain the changes visually.

# Type of Changes

- [ ] Bug fix
- [ ] New feature
- [ ] Enhancement
- [ ] Documentation
- [ ] Refactor

# Checklist

- [ ] My code follows the project's coding style
- [ ] I have added or updated tests
- [ ] I have updated the documentation if necessary
- [ ] All new and existing tests passed
- [ ] The branch is up-to-date with the base branch

# Additional Context

Add any other information or context that may be useful for reviewers.
