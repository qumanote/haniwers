# Title: [Brief and descriptive title of the feature]

# Description

A detailed description of the new feature. Include the motivation behind the feature and its intended outcome.

# Related Issues

Closes #[issue_number]

# Feature Design

Explain the design of the feature, including any architectural or UI changes.

# How to Test

Provide a step-by-step guide on how to test the new feature. Include any relevant test cases.

1. Step one
2. Step two
3. Step three

# Screenshots (if applicable)

Add screenshots, diagrams, or GIFs to help illustrate the feature.

# Checklist

- [ ] Feature is fully implemented
- [ ] All new and existing tests pass
- [ ] Documentation has been updated
- [ ] The branch is up-to-date with the base branch

# Additional Context

Include any additional context or information relevant to the feature.
