# ログブック

## 古墳測定

- [](./logbook/run1/20230809_run1_kofun.ipynb) : 早稲田本庄の生徒たちの測定
- [](./logbook/run20/20230809_run20_kofun.ipynb) : KEK／山形大／名古屋大などの合同測定
- [](./logbook/run21/20230904_run21_kofun.ipynb)

## テスト測定

- [](./logbook/run32/20230819_run32_kamiokalab.ipynb)
- [](./logbook/run34/20230822_run34_airplane.ipynb)
- [](./logbook/run50/20240507_run50.ipynb)
- [](./logbook/run65/20240520_run65.ipynb)

## 天頂角分布測定

- [](./logbook/run85/run85_zenith.ipynb)
- [](./logbook/run95/run95_zenith.ipynb)

## スレッショルド測定

- [](./logbook/run24/20230815_run24_threshold_scan.ipynb)
- [](./logbook/run26/20230816_run26_noise_measurement.ipynb)
- [](./logbook/run28/20230817_run28_noise_temperature.ipynb)
- [](./logbook/run35/20230825_run35_threshold_scan.ipynb)
- [](./logbook/run76/20240529_run76.ipynb)



## 解析チュートリアル

OSECHIでデータを取得したり、解析したりするときの基本となるPython/Pandas操作について整理しています。
コンテンツは随時追加します。

1. [](./tutorials/00-run.md)
2. [](./tutorials/01-read_data.ipynb)
3. [](./tutorials/02-resample_data.ipynb)
4. [](./tutorials/03-pyserial.ipynb)
