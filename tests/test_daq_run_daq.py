from unittest.mock import MagicMock, patch, mock_open
from pathlib import Path


# モック用の関数
def mock_get_savef_with_timestamp(daq, nfile):
    return Path(f"dummy_path_{nfile}.csv")


def mock_loop_and_save_events(fname, port, max_rows, suffix):
    pass


def mock_loop_and_save(fname, generator):
    pass


@patch("haniwers.daq.mkdir_saved")
@patch("haniwers.daq.get_savef_with_timestamp", side_effect=mock_get_savef_with_timestamp)
@patch("haniwers.daq.loop_and_save_events", side_effect=mock_loop_and_save_events)
@patch("haniwers.daq.logger")
@patch("pathlib.Path.open", new_callable=mock_open)
def test_run_daq(
    mock_file_open,
    mock_logger,
    mock_loop_and_save_events,
    mock_get_savef_with_timestamp,
    mock_mkdir_saved,
):
    daq = MagicMock()
    daq.max_files = 3
    daq.max_rows = 5
    port = MagicMock()

    from haniwers.daq import run_daq

    run_daq(port, daq)

    # ディレクトリ作成が呼ばれているか確認
    mock_mkdir_saved.assert_called_once_with(daq)
    # ファイル保存ループの確認
    assert mock_get_savef_with_timestamp.call_count == daq.max_files
    assert mock_loop_and_save_events.call_count == 0
    # ログメッセージの確認
    assert mock_logger.info.call_count == 2 * daq.max_files
    # assert mock_logger.success.call_count == daq.max_files
    # ファイルオープンが呼ばれているか確認
    assert mock_file_open.call_count == daq.max_files
    # ファイルハンドルを取得して、書き込みが行われていることを確認
    # handle = mock_file_open()
    # assert handle.write.call_count == 0  # mock_loop_and_save_events で何も書き込まないため
