from haniwers.config import UserSettings

from pathlib import Path

# "../sandbox/hnw.toml"
from . import SETTINGS_TOML


def test_settings() -> None:
    """UserSettingsクラスの単体テスト"""
    test = UserSettings(load_from=str(SETTINGS_TOML))
    expected = ["default", "device", "daq", "scan", "threshold", "loguru", "rules"]
    assert list(test.sections) == expected


def test_daq_settings() -> None:
    """DAQ用の設定を取得する単体テスト"""
    test = UserSettings(load_from=str(SETTINGS_TOML))

    expected = [
        "saved",
        "suffix",
        "skip",
        "max_rows",
        "port",
        "baudrate",
        "timeout",
        "prefix",
        "max_files",
    ]

    settings = test.get_daq_settings()
    keys = list(settings.keys())
    assert keys == expected


def test_scan_settings() -> None:
    """スレッショルド測定の設定を取得する単体テスト"""
    test = UserSettings(load_from=str(SETTINGS_TOML))

    expected = [
        "saved",
        "suffix",
        "skip",
        "max_rows",
        "port",
        "baudrate",
        "timeout",
        "prefix",
        "max_files",
        "logs",
        "scan",
        "fit",
    ]

    settings = test.get_scan_settings()
    keys = list(settings.keys())
    assert keys == expected
