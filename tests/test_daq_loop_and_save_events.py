from pathlib import Path
from unittest.mock import MagicMock, patch, mock_open
from haniwers.mimic import FakeEvent
from haniwers.daq import (
    RealEvent,
    _loop_and_save_events,
    _loop_events_for_duration,
    _loop_events_for_rows,
    loop_and_save,
)


@patch("pathlib.Path.open", new_callable=mock_open)
def test_loop_and_save_events(mock_open):
    # ファイルモック
    fname = Path("remove_this_file_if_exists.csv")

    # Serialオブジェクトのモック
    port = MagicMock()
    port.readline().decode.return_value = FakeEvent().to_mock_string()

    # イベント取得の回数
    max_rows = 5
    suffix = ".csv"
    events = list(_loop_and_save_events(fname, port, max_rows=max_rows, suffix=suffix))

    # 検証
    assert len(events) == max_rows
    mock_open.assert_called_once_with("x")
    handle = mock_open()
    assert handle.write.call_count == max_rows
    assert handle.flush.call_count == max_rows

    for event in events:
        assert isinstance(event, RealEvent)
        assert isinstance(event.top, int)
        assert isinstance(event.mid, int)
        assert isinstance(event.btm, int)
        assert isinstance(event.adc, int)
        assert isinstance(event.tmp, float)
        assert isinstance(event.atm, float)
        assert isinstance(event.hmd, float)


@patch("pathlib.Path.open", new_callable=mock_open)
def test_loop_and_save_rows(mock_open):
    # ファイルモック
    fname = Path("remove_this_file_if_exists.csv")

    # Serialオブジェクトのモック
    mock_port = MagicMock()
    mock_port.readline().decode.return_value = FakeEvent().to_mock_string()

    # イベント取得の回数
    max_rows = 5
    generator = _loop_events_for_rows(port=mock_port, max_rows=max_rows)
    events = list(loop_and_save(fname, generator=generator))

    # 検証
    assert len(events) == max_rows
    mock_open.assert_called_once_with("x")
    handle = mock_open()
    assert handle.write.call_count == max_rows
    assert handle.flush.call_count == max_rows

    for event in events:
        assert isinstance(event, RealEvent)
        assert isinstance(event.top, int)
        assert isinstance(event.mid, int)
        assert isinstance(event.btm, int)
        assert isinstance(event.adc, int)
        assert isinstance(event.tmp, float)
        assert isinstance(event.atm, float)
        assert isinstance(event.hmd, float)


@patch("pathlib.Path.open", new_callable=mock_open)
def test_loop_and_save_duration(mock_open):
    # ファイルモック
    fname = Path("remove_this_file_if_exists.csv")

    # Serialオブジェクトのモック
    mock_port = MagicMock()
    mock_port.readline().decode.return_value = FakeEvent().to_mock_string()

    # イベント取得の時間
    max_duration = 1
    generator = _loop_events_for_duration(port=mock_port, max_duration=max_duration)
    events = list(loop_and_save(fname, generator=generator))

    # 検証
    # assert len(events) == max_rows
    mock_open.assert_called_once_with("x")
    # handle = mock_open()
    # assert handle.write.call_count == max_duration
    # assert handle.flush.call_count == max_duration

    for event in events:
        assert isinstance(event, RealEvent)
        assert isinstance(event.top, int)
        assert isinstance(event.mid, int)
        assert isinstance(event.btm, int)
        assert isinstance(event.adc, int)
        assert isinstance(event.tmp, float)
        assert isinstance(event.atm, float)
        assert isinstance(event.hmd, float)
