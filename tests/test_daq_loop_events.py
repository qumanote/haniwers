"""イベント取得ループのテスト

:Arrange:
- `port (MagicMock)`: Serialオブジェクトのモック
- `port.readline().decode.return_value`: OSECHIから読み出すデータのモック（文字列）

:Act:
- ``daq._loop_events_for_rows``: 回数指定のイベント取得ループ
- ``daq._loop_events_for_time``: 時間指定のイベント取得ループ

:Assersion:
- 得られたデータの型チェック
"""

from unittest.mock import MagicMock
from haniwers.mimic import FakeEvent
from haniwers.daq import RealEvent, _loop_events_for_rows, _loop_events_for_duration


def test_loop_events_for_rows():
    port = MagicMock()
    port.readline().decode.return_value = FakeEvent().to_mock_string()

    rows = 5
    events = list(_loop_events_for_rows(port, rows))

    # 検証
    assert len(events) == rows
    for event in events:
        assert isinstance(event, RealEvent)
        assert isinstance(event.top, int)
        assert isinstance(event.mid, int)
        assert isinstance(event.btm, int)
        assert isinstance(event.adc, int)
        assert isinstance(event.tmp, float)
        assert isinstance(event.atm, float)
        assert isinstance(event.hmd, float)


def test_loop_events_for_duration():
    port = MagicMock()
    port.readline().decode.return_value = FakeEvent().to_mock_string()

    duration = 1
    events = list(_loop_events_for_duration(port, duration))

    # 検証
    # assert len(events) ==
    for event in events:
        assert isinstance(event, RealEvent)
        assert isinstance(event.top, int)
        assert isinstance(event.mid, int)
        assert isinstance(event.btm, int)
        assert isinstance(event.adc, int)
        assert isinstance(event.tmp, float)
        assert isinstance(event.atm, float)
        assert isinstance(event.hmd, float)
