from pathlib import Path

PYTEST_DIR = Path("tests/")
if not PYTEST_DIR.exists():
    PYTEST_DIR = Path(".")

TEST_DATA_DIR = Path("data/test_data/")
if not TEST_DATA_DIR.exists():
    TEST_DATA_DIR = Path("../data/test_data/")

CONFIG_TOML = PYTEST_DIR / Path("config.toml")
DAQ_TOML = PYTEST_DIR / Path("daq.toml")
SCAN_TOML = PYTEST_DIR / Path("scan.toml")
SETTINGS_TOML = PYTEST_DIR / Path("hnw.toml")
RUNS_CSV = PYTEST_DIR / Path("runs.csv")
