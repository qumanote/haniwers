from haniwers.config import RunData


def test_rundata() -> None:
    """RunDataクラスの単体テスト

    ラン番号は必須。
    他はデフォルト値。
    """
    run = RunData(1)

    assert run.run_id == 1
    assert run.description == ""
    assert run.read_from == ""
    assert run.srcf == "*.csv"
    assert run.interval == 600
    assert run.skip is False
    # assert run.nfiles == 0
    assert run.raw2gz == ""
    assert run.raw2csv == ""
    assert run.query == ""


def test_run1() -> None:
    """テスト用データを使ってRunDataクラスを確認

    data/test_data/ にテスト用データを保存した。
    """
    run = RunData(
        run_id=1,
        read_from="data/test_data/20220425_run1/",
        srcf="*.dat",
    )

    assert run.run_id == 1
    assert run.read_from == "data/test_data/20220425_run1/"
    assert run.srcf == "*.dat"
    assert run.nfiles == 5
