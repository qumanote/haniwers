from haniwers.config import RunManager

import pandas as pd

# "./run.csv"
# "../data/run.csv"
from . import RUNS_CSV


def test_settings() -> None:
    """RunManagerクラスの単体テスト"""
    test = RunManager(load_from=str(RUNS_CSV), is_test=True)
    assert isinstance(test.data, pd.DataFrame)
