from unittest.mock import MagicMock
from haniwers.mimic import FakeEvent
from haniwers.daq import RealEvent, _read_event


def test_read_serial_data_as_event():
    # MagicMockを作成
    port = MagicMock()
    port.readline().decode.return_value = FakeEvent().to_mock_string()

    # イベントのダミーを取得
    event = _read_event(port)

    # 検証
    assert isinstance(event, RealEvent)
    assert isinstance(event.top, int)
    assert isinstance(event.mid, int)
    assert isinstance(event.btm, int)
    assert isinstance(event.adc, int)
    assert isinstance(event.tmp, float)
    assert isinstance(event.atm, float)
    assert isinstance(event.hmd, float)
