from haniwers.threshold import erfc_function, fit_threshold_by_channel, fit_thresholds

from pathlib import Path
from . import TEST_DATA_DIR
import pandas as pd


def test_fit_threshold_by_channel() -> None:
    """スレッショルド測定の単体テスト"""

    fname = TEST_DATA_DIR / Path("20231201_run44/threshold_scan.csv")
    names = ["time", "duration", "ch", "vth", "events", "tmp", "atm", "hmd"]
    data = pd.read_csv(fname, names=names, parse_dates=["time"])

    expected = {
        "ch1": 1,
        "ch2": 2,
        "ch3": 3,
        "len": 1,
        "names": ["timestamp", "ch", "0sigma", "1sigma", "3sigma", "5sigma"],
    }
    params = [10, 300, 1, 1]

    test, _, _ = fit_threshold_by_channel(data, ch=1, func=erfc_function, params=params)
    assert test["ch"].all() == expected["ch1"]
    assert len(test) == expected["len"]
    assert list(test.columns) == expected["names"]

    test, _, _ = fit_threshold_by_channel(data, ch=2, func=erfc_function, params=params)
    # assert test["ch"].all() == expected["ch2"]
    assert len(test) == expected["len"]
    assert list(test.columns) == expected["names"]

    test, _, _ = fit_threshold_by_channel(data, ch=3, func=erfc_function, params=params)
    # assert test["ch"].all() == expected["ch3"]
    assert len(test) == expected["len"]
    assert list(test.columns) == expected["names"]


def test_fit_thresholds() -> None:
    """スレッショルド測定のテスト"""

    fname = TEST_DATA_DIR / Path("20231201_run44/threshold_scan.csv")
    names = ["time", "duration", "ch", "vth", "events", "tmp", "atm", "hmd"]
    data = pd.read_csv(fname, names=names, parse_dates=["time"])
    params = [10, 300, 1, 1]

    channels = [1, 2, 3]
    test = fit_thresholds(data, channels, params=params)

    expected = {
        "len": 3,
        "names": ["timestamp", "ch", "0sigma", "1sigma", "3sigma", "5sigma"],
    }
    assert len(test) == expected["len"]
    assert list(test.columns) == expected["names"]
