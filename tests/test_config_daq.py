from haniwers.config import Daq

from . import DAQ_TOML


def test_daq() -> None:
    """Daqクラスの単体テスト"""
    test = Daq()

    expected = {
        "saved": ".",
        "prefix": "data",
        "suffix": ".csv",
        "skip": 10,
        "max_rows": 10000,
        "max_files": 100,
        "quiet": False,
        "append": False,
        "device": "/dev/ttyUSB0",
        "baudrate": 115200,
        "timeout": 1000,
        "fname_logs": "threshold_logs.csv",
        "fname_scan": "threshold_scan.csv",
    }

    assert test.saved == expected["saved"]
    assert test.prefix == expected["prefix"]
    assert test.suffix == expected["suffix"]
    assert test.skip == expected["skip"]
    assert test.max_rows == expected["max_rows"]
    assert test.max_files == expected["max_files"]
    assert test.quiet == expected["quiet"]
    assert test.append == expected["append"]
    assert test.device == expected["device"]
    assert test.baudrate == expected["baudrate"]
    assert test.timeout == expected["timeout"]
    assert test.fname_logs == expected["fname_logs"]
    assert test.fname_scan == expected["fname_scan"]


def test_daq_toml():
    """DaqクラスにTOMLを読み込ませたテスト"""
    test = Daq()

    test.load_toml(DAQ_TOML)

    expected = {
        "saved": "pytest",
        "prefix": "pytest_data",
        "suffix": ".csv",
        "skip": 10,
        "max_rows": 100,
        "max_files": 100,
        "quiet": True,
        "append": True,
        "device": "/dev/ttyUSB0",
        "baudrate": 115200,
        "timeout": 100,
        "fname_logs": "pytest_threshold_logs.csv",
        "fname_scan": "pytest_threshold_scan.csv",
    }

    assert test.saved == expected["saved"]
    assert test.prefix == expected["prefix"]
    assert test.suffix == expected["suffix"]
    assert test.skip == expected["skip"]
    assert test.max_rows == expected["max_rows"]
    assert test.max_files == expected["max_files"]
    assert test.quiet == expected["quiet"]
    assert test.append == expected["append"]
    assert test.device == expected["device"]
    assert test.baudrate == expected["baudrate"]
    assert test.timeout == expected["timeout"]
    assert test.fname_logs == expected["fname_logs"]
    assert test.fname_scan == expected["fname_scan"]
