---
marp: true
theme: gaia
---

# テスト測定（Run3 - 5）のまとめ

作成日：2022年05月26日
更新日：2022年06月01日

KEK髙橋将太

1. 測定日時とヒット数の推移
1. TOPレイヤーのADC分布の比較

---

# 測定データ

1. `Run1` : 古墳・奥壁（4 月 25 日〜27 日） ／ `3099415` events
1. `Run2` : 古墳・入口（4 月 25 日〜30 日） ／ `4280847` events
1. `Run3` : KEK・室内（5 月 18 日） ／ `84690` events
1. `Run4` : KEK・室内（5 月 19 日） ／ `85106` events
1. `Run5` : KEK・屋外（5月23日〜27日）／ `1009048` events

- **古墳** （Run1, 2）: ソーラーパネル
- **KEK室内** （Run3, 4）: バッテリー（ソーラーAC電源はなし）
- **KEK屋外** （Run5）: ソーラーパネル

---

# イベントカテゴリ


- 貫通したイベント（``key == 'hit3'``）のみを取り出す
  - `hit3` = `top > 0 and mid > 0 and btm > 0`

除外したデータ期間

- ``Run2`` : 4月30日のデータ（``time < '2022-04-30'`` ）
- ``Run5`` : 5月27日9時以降のデータ（``time < '2022-05-27T09:00:00'`` ）

---

# 取得したデータを確認する

- ランごとのヒット数の推移を確認した
  - ``600``秒でリサンプリングしたデータを使った
- x軸 : 測定日時（``x="time"``）
- y軸 : ヒット数（``y="hit"``）；範囲は ``(-10, 150)``で揃えた
- z軸 : 気温（``color="tmp_mean"``）


---
# Run1

![bg fit](overview_time_hit_run1.png)

---
# Run2

![bg fit](overview_time_hit_run2.png)

---
# Run3

![bg fit](overview_time_hit_run3.png)

---
# Run4

![bg fit](overview_time_hit_run4.png)

---
# Run5

![bg fit](overview_time_hit_run5.png)


---

# 古墳測定とKEK測定の比較

- 古墳測定（Run1, Run2）とKEK測定（Run3, Run4, Run5）
- 古墳測定では **DAQ開始時** にヒット数（レート）が多かったが、KEK測定ではそれが**見えない**
- Run5（KEK屋外）から、温度とヒット数に関係がありそうなことが分かる
  - **温度が高い**と**ヒット数が減る**

---

# Run1

![bg fit](top_adc_run1.png)

---

# Run2

![bg fit](top_adc_run2.png)


---

# Run3

![bg fit](top_adc_run3.png)


---

# Run4

![bg fit](top_adc_run4.png)


---

# Run5

![bg fit](top_adc_run5.png)

---

# ランごとの比較

- （重ね描きができなかったので、ページをスクロールして、睨めっこしてください）
- Run1,2 と Run3,4,5 を見比べてもなんともいえない
- 閾値を **Run3 > Run4** とし。小さいADC値がRun4で増えてるのがわかる。このあたりに、信号とノイズの境目があるのかな？
- ``Run5``ではそれっぽいピーク（とテール）があるが、ピーク前（1100あたり）にある固まりも気になる。あとRun3,4の分布とも見た目が異なる。
- OSECHIの**電源のON/OFF**でADCのぺデスタルは変化するのか？

---

# ランごとに重ね描きたい

- ランごとのエントリー数が異なるので、エントリー数で規格化する
- ``pandas``と``plotly express``を使って、重ね描く方法がわからなかった
  - ``px.histogram``の``histnorm="probability"``がそれっぽい
  - ``Graph Object``を重ね描きする方法がわからない

```c++
// ROOTだとこんな感じの操作（だったはず）
h1->Scale(1. / h1->GetEntries())
h2->Scale(1. / h2->GetEntries())

h1->Draw()
h2->Draw("same")
```


---

# バックアップ：DAQの改造 ``run4.py``

- 日時モジュールを変更（ ``import pendulum`` ）
- 標準出力用のモジュールを追加（``from icecream import ic``）
- 進捗バー用のモジュールを追加（``from tqdm import tqdm``）
- **with**構文 ／ ``try ... except`` を利用
  - ファイル／ポートの **open/close** 処理をPython風に書き直した
  - ``Ctrl-C``をキャッチして、きれいに停止できるようにした

---
# バックアップ：DAQのオプション

```python
$ python3 run4.py
```

- **--saved** : 保存するディテクトリ名；実行日の日付
- **--prefix** : ファイル名を設定；"osechi_data"
- **--suffix** : 拡張子を設定；"**.csv**" / "**.dat**"から選択
- **--max-rows ROWS** : 1ファイルあたりの行数を設定；10000行
- **--max-files FILES** : 最大ファイル数を設定；100ファイル
- **--append** : 既存のファイルへの追記；False

---


# バックアップ：ノートブックを追加

- 📗 ``notebooks/overview.ipynb``
  - 📊 ``overview_time_hit_run[1-5].png``
- 📗 ``notebooks/top_adc.ipynb``
  - 📊 ``top_adc_run[1-5].png``
