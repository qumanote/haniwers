#include <SPI.h>

#define LEDpin1 14
#define LEDpin2 26
#define LEDpin3 33

#define Detectpin1 12
#define Detectpin2 19
#define Detectpin3 27

static const uint32_t GPIO_IN_REG_MASK = 0b00001000000010000001000000000000;
int signal1, signal2, signal3;

static const int spiClk = 100000; // 1 MHz

//uninitalised pointers to SPI objects
SPIClass * vspi = NULL;

//Interrupt
volatile boolean pushed = false;

const int swich_pin = 0;

void IRAM_ATTR onPushed() {
    pushed = true;
}

//ADC
//int Trg = 27;
int ADC_IN = 32;

void setup(void) {
//ADC setup
  Serial.begin(115200);
//  Serial.begin(9600);
  pinMode(ADC_IN, ANALOG);
  //pinMode(Trg, INPUT);

//SPI setup
  vspi = new SPIClass(VSPI);

  //clock miso mosi ss

  //initialise vspi with default pins
  //SCLK = 18, MISO = 19, MOSI = 23, SS = 5
  vspi->begin();

  //pinMode(5, OUTPUT); //VSPI SS

  pinMode(5, OUTPUT); //VSPI SS DAC 0
  pinMode(13, OUTPUT); //VSPI SS DAC 1
  pinMode(15, OUTPUT); //VSPI SS DAC 2

  attachInterrupt(swich_pin, onPushed, FALLING);


  pinMode( LEDpin1, OUTPUT );
  pinMode( LEDpin2, OUTPUT );
  pinMode( LEDpin3, OUTPUT );

  pinMode( Detectpin1, INPUT_PULLDOWN );
  pinMode( Detectpin2, INPUT_PULLDOWN );
  pinMode( Detectpin3, INPUT_PULLDOWN );

  gpio_set_pull_mode( GPIO_NUM_14, GPIO_PULLDOWN_ONLY );
  gpio_set_pull_mode( GPIO_NUM_27, GPIO_PULLDOWN_ONLY );
  gpio_set_pull_mode( GPIO_NUM_26, GPIO_PULLDOWN_ONLY );

  //Serial.begin( 9600 );
  delay( 3000 );

}

// the loop function runs over and over again until power down or reset
void loop() {
  digitalWrite(5,HIGH);
  digitalWrite(13,HIGH);
  digitalWrite(15,HIGH);
  if(Serial.available()>0){
    int ch = Serial.read();
    byte val1 = Serial.read();
    byte val2 = Serial.read();
    delay(100);
    if(val1 == 0xff){
      Serial.println("dame");
      while(Serial.available()){
        Serial.read();
      }
    }
    else{
      Serial.println(ch,DEC);
      Serial.println(val1,BIN);
      Serial.println(val2,BIN);
      delay(100);
      vspiCommand(ch,val1,val2);
      while(Serial.available()){
        Serial.read();
      }
    }
  }
/*
  if(pushed==true){
    vspiCommand0();
    vspiCommand1();
    vspiCommand2();
    pushed=false;
  }
*/

  //signal1 = digitalRead( Detectpin1 );
  //signal2 = digitalRead( Detectpin2 );
  //signal3 = digitalRead( Detectpin3 );

  for( int ii = 0; ii<10; ii++){
uint32_t gpio_in_reg = *((uint32_t*)0x3FF4403C);
  signal1 += ((gpio_in_reg & GPIO_IN_REG_MASK)>>12)&1;
  signal2 += ((gpio_in_reg & GPIO_IN_REG_MASK)>>19)&1;
  signal3 += ((gpio_in_reg & GPIO_IN_REG_MASK)>>27)&1;
  }

  if( (signal1 >0 ) || (signal2 > 0) || (signal3 > 0) ){

     Serial.print( signal1 );
     Serial.print( " " );
     Serial.print( signal2 );
     Serial.print( " " );
     Serial.println( signal3 );
     digitalWrite( LEDpin1, signal1 );
     digitalWrite( LEDpin2, signal2 );
     digitalWrite( LEDpin3, signal3 );
     delay( 50 );
     digitalWrite( LEDpin1, LOW );
     digitalWrite( LEDpin2, LOW );
     digitalWrite( LEDpin3, LOW );

  }
  signal1 = 0; signal2 = 0; signal3 = 0;
/*
  if(signal1 == HIGH) {
    int sensorValue = analogRead(ADC_IN);
    Serial.println(sensorValue);

    pinMode(ADC_IN, OUTPUT);
    digitalWrite(ADC_IN, LOW);
    pinMode(ADC_IN, ANALOG);
    delay(1);
  }
*/
}
//Dn=Vout/(2*1.024)
void vspiCommand(int channel, byte data1, byte data2){
  int setCH=5;
  if(channel==2) setCH=13;
  if(channel==3) setCH=15;

  vspi->beginTransaction(SPISettings(spiClk, MSBFIRST, SPI_MODE0));
  digitalWrite(setCH, LOW); //pull SS slow to prep other end for transfer
  vspi->transfer(data1);
  vspi->transfer(data2);
  digitalWrite(setCH, HIGH); //pull ss high to signify end of data transfer
  vspi->endTransaction();

}

void vspiCommand0() {

  //String bytedata = String(Serial.read(),BIN);

  byte data1 = 0b00010100; //V3 #1 Voff = 1044mV, Vth/4 = 1084/4 = 269 --> 100 00 1111
  byte data2 = 0b00111100;

  //use it as you would the regular arduino SPI API
  vspi->beginTransaction(SPISettings(spiClk, MSBFIRST, SPI_MODE0));
  digitalWrite(5, LOW); //pull SS slow to prep other end for transfer
  vspi->transfer(data1);
  vspi->transfer(data2);
  digitalWrite(5, HIGH); //pull ss high to signify end of data transfer
  vspi->endTransaction();

}

void vspiCommand1() {

  byte data1 = 0b00010100; //V3 #1 Voff = 1128mV, Vth/4 = 1200/4 = 300 --> 1 00 10 1100
  byte data2 = 0b10011100;

  //use it as you would the regular arduino SPI API
  vspi->beginTransaction(SPISettings(spiClk, MSBFIRST, SPI_MODE0));
  digitalWrite(13, LOW); //pull SS slow to prep other end for transfer
  vspi->transfer(data1);
  vspi->transfer(data2);
  digitalWrite(13, HIGH); //pull ss high to signify end of data transfer
  vspi->endTransaction();

}

void vspiCommand2() {

  byte data1 = 0b00010100; //V3 #1 Voff = 980mV, Vth/4 = 1070/4 = 267 --> 1 00 00 1011
  byte data2 = 0b00011000;

  //use it as you would the regular arduino SPI API
  vspi->beginTransaction(SPISettings(spiClk, MSBFIRST, SPI_MODE0));
  digitalWrite(15, LOW); //pull SS slow to prep other end for transfer
  vspi->transfer(data1);
  vspi->transfer(data2);
  digitalWrite(15, HIGH); //pull ss high to signify end of data transfer
  vspi->endTransaction();

}
